# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Lothar Krause and Sergi Blanch-Torne and Emilio Morales"
__maintainer__ = "Emilio Morales"
__copyright__ = "Copyright 2015, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

"""Device Server to control the Alba's Linac manufactured by Thales."""

__all__ = ["LinacData", "LinacDataClass", "main"]

__docformat__ = 'restructuredtext'

try:
    import tango
# suse11.1 backwards compatibility
except Exception:
    import PyTango as tango

import sys
# Add additional import
# PROTECTED REGION ID(linacdata.additionnal_import) ---
import fcntl
from numpy import uint16, uint8, float32, int16
from os import path
import pprint
import psutil
import socket
import struct
import time
try:
    from . import tcpblock
# suse11.1 backwards compatibility
except ValueError:  # Attempted relative import in non-package
    import tcpblock
import threading
import traceback
from types import StringType

try:
    from .constants import *
    from .linacattrs import (LinacException, CommandExc, AttrExc,
                             binaryByte, hex_dump)
    from .linacattrs import (EnumerationAttr, EnumerationParameter, PLCAttr,
                             InternalAttr, MeaningAttr, AutoStopAttr,
                             AutoStopParameter, HistoryAttr, GroupAttr,
                             LogicAttr)
    from .linacattrs.linacfeatures import (CircularBuffer, HistoryBuffer,
                                           EventCtr)
# suse11.1 backwards compatibility
except ValueError:  # Attempted relative import in non-package
    from constants import *
    from linacattrs import (LinacException, CommandExc, AttrExc,
                             binaryByte, hex_dump)
    from linacattrs import (EnumerationAttr, EnumerationParameter, PLCAttr,
                            InternalAttr, MeaningAttr, AutoStopAttr,
                            AutoStopParameter, HistoryAttr, GroupAttr,
                            LogicAttr)
    from linacattrs.linacfeatures import (CircularBuffer, HistoryBuffer,
                                          EventCtr)


class release:
    author = 'Lothar Krause &'\
             ' Sergi Blanch-Torne <sblanch@cells.es> &'\
             ' Emilio Morales <emorales@cells.es>'
    hexversion = (((MAJOR_VERSION << 8) | MINOR_VERSION) << 8) | BUILD_VERSION
    __str__ = lambda self: hex(hexversion)


TYPE_MAP = {tango.DevUChar: ('B', 1),
            tango.DevShort: ('h', 2),
            tango.DevFloat: ('f', 4),
            tango.DevDouble: ('f', 4),
            # the PLCs only use floats of 4 bytes
            }


def john(sls):
    """
used to encode the messages shown for each state code
    """
    if type(sls) == dict:
        return '\n'+''.join('{0:d}:{1}\n'.format(t, sls[t])
                            for t in sls.keys())
    else:
        return '\n'+''.join('{0:d}:{1}\n'.format(i, t)
                            for i, t in enumerate(sls))


def latin1(x):
    return x.decode('utf-8').replace(u'\u2070', u'\u00b0').\
        replace(u'\u03bc', u'\u00b5').encode('latin1')


class AttrList(object):
    '''Manages dynamic attributes and contains methods for conveniently adding
       attributes to a running TANGO device.
    '''
    def __init__(self, device):
        super(AttrList, self).__init__()
        self.impl = device
        self._db20_size = self.impl.ReadSize-self.impl.WriteSize
        self._db22_size = self.impl.WriteSize
        self.alist = list()
        self.locals_ = {}
        self._relations = {}
        self._builder = None
        self._fileParsed = threading.Event()
        self._fileParsed.clear()

        self.globals_ = globals()
        self.globals_.update({
            'DEVICE': self.impl,
            'LIST': self,
            'Attr': self.add_AttrAddr,
            'AttrAddr': self.add_AttrAddr,
            'AttrBit': self.add_AttrAddrBit,
            'GrpBit': self.add_AttrGrpBit,
            'AttrLogic': self.add_AttrLogic,
            'AttrRampeable': self.add_AttrRampeable,
            'AttrPLC': self.add_AttrPLC,
            'AttrEnumeration': self.add_AttrEnumeration,
        })

    def add_Attr(self, name, T, rfun=None, wfun=None, label=None, desc=None,
                 minValue=None, maxValue=None, unit=None, format=None,
                 memorized=False, logLevel=None, xdim=0, *args, **kwargs):
        if wfun:
            if xdim == 0:
                attr = tango.Attr(name, T, tango.READ_WRITE)
            else:
                self.impl.error_stream("Not supported write attribute in "
                                       "SPECTRUMs. {0} will be readonly."
                                       "".format(name))
                attr = tango.SpectrumAttr(name, T, tango.READ_WRITE, xdim)
        else:
            if xdim == 0:
                attr = tango.Attr(name, T, tango.READ)
            else:
                attr = tango.SpectrumAttr(name, T, tango.READ, xdim)
        if logLevel is not None:
            self.impl._getAttrStruct(name).logLevel = logLevel
        aprop = tango.UserDefaultAttrProp()
        if unit is not None:
            aprop.set_unit(latin1(unit))
        if minValue is not None:
            aprop.set_min_value(str(minValue))
        if maxValue is not None:
            aprop.set_max_value(str(maxValue))
        if format is not None:
            attrStruct = self.impl._getAttrStruct(name)
            attrStruct['format'] = str(format)
            aprop.set_format(latin1(format))
        if desc is not None:
            aprop.set_description(latin1(desc))
        if label is not None:
            aprop.set_label(latin1(label))
        if memorized:
            attr.set_memorized()
            attr.set_memorized_init(True)
            self.impl.info_stream("Making {0} memorized ({1},{2})".format(
                name, attr.get_memorized(), attr.get_memorized_init()))
        attr.set_default_properties(aprop)

        rfun = AttrExc(rfun)
        try:
            if wfun:
                wfun = AttrExc(wfun)
        except Exception as e:
            self.impl.error_stream("Attribute {0} build exception: {1}".format(
                                   name, e))

        self.impl.add_attribute(attr, r_meth=rfun, w_meth=wfun)
        if name in self.impl._plcAttrs and \
                EVENTS in self.impl._plcAttrs[name]:
            self.impl.set_change_event(name, True, False)
        elif name in self.impl._internalAttrs and \
                EVENTS in self.impl._internalAttrs[name]:
            self.impl.set_change_event(name, True, False)
        self.alist.append(attr)
        return attr

    def __mapTypes(self, attrType):
        # ugly hack needed for SOLEILs archiving system
        if attrType == tango.DevFloat:
            return tango.DevDouble
        elif attrType == tango.DevUChar:
            return tango.DevShort
        else:
            return attrType

    def add_AttrAddr(self, name, T, read_addr=None, write_addr=None,
                     meanings=None, qualities=None, events=None,
                     formula=None, label=None, desc=None,
                     readback=None, setpoint=None, switch=None,
                     IamChecker=None, minValue=None, maxValue=None,
                     *args, **kwargs):
        '''This method is a most general builder of dynamic attributes, for RO
           as well as for RW depending on if it's provided a write address.
           There are other optional parameters to configure some special
           characteristics.

           With the meaning parameter, a secondary attribute to the give one
           using the name is created (with the suffix *_Status and They share
           other parameters like qualities and events). The numerical attribute
           can be used in formulas, alarms and any other machine-like system,
           but this secondary attribute is an DevString who concatenates the
           read value with an string specified in a dictionary in side this
           meaning parameter in order to provide a human-readable message to
           understand that value.

           All the Tango attributes have characteristics known as qualities
           (like others like format, units, and so on) used to provide a 5
           level state-like information. They can by 'invalid', 'valid',
           'changing', 'warning' or 'alarm'. With the dictionary provided to
           the parameter qualities it can be defined some ranges or some
           discrete values. The structure splits between this two situations:
           - Continuous ranges: that is mainly used for DevDoubles but also
             integers. As an example of the dictionary:
             - WARNING:{ABSOLUTE:{BELOW:15,ABOVE:80}}
               This will show VALID quality between 15 and 80, but warning
               if in absolute terms the read value goes out this thresholds.
           - Discrete values: that is used mainly in the state-like attributes
             and it will establish the quality by an equality. As example:
             - ALARM:[0],
               WARNING:[1,2,3,5,6,7],
               CHANGING:[4]
               Suppose a discrete attribute with values between 0 and 8. Then
               when value is 8 will be VALID, between 0 and 7 will be WARNING
               with the exception at 4 that will show CHANGING.

           Next of the parameters is events and with this is configured the
           behaviour of the attribute to emit events. Simply by passing a
           dictionary (even void like {}) the attribute will be configured to
           emit events. In this simplest case events will be emitted if the
           value has changed from last reading. But for DevDouble is used a
           key THRESHOLD to indicate that read changes below it will not
           produce events (like below the format representation). For such
           thing is used a circular buffer that collects reads and its mean
           is used to compare with a new reading to decide if an event has to
           be emitted or not.

           Another parameter is the formula. This is mainly used with the
           DevBooleans but it's possible for any other. It's again a dictionary
           with two possible keys 'read' and/or 'write' and their items shall
           be assessed strings in running time that would 'transform' a
           reading.

           The set of arguments about readback, setpoint and switch are there
           to store defined relations between attributes. That is, to allow the
           setpoint (that has a read and write addresses) to know if there is
           another read only attribute that does the measure of what the
           setpoint sets. Also this readback may like to know about the
           setpoint and if the element is switch on or off.

           In the attribute description, one key argument (default None) is:
           'IamChecker'. It is made to, if it contains a list of valid read
           values, add to the tcpblock reader to decide if a received block
           has a valid structure or not.
        '''
        self.__traceAttrAddr(name, T, readAddr=read_addr, writeAddr=write_addr)
        tango_T = self.__mapTypes(T)
        try:
            read_addr = self.__check_addresses_and_block_sizes(
                name, read_addr, write_addr)
        except IndexError:
            return
        is_fresh_created = self._prepareAttribute(
            name, T, readAddr=read_addr, writeAddr=write_addr, formula=formula,
            readback=readback, setpoint=setpoint, switch=switch, label=label,
            description=desc, minValue=minValue, maxValue=maxValue,
            *args, **kwargs)
        if is_fresh_created:
            rfun = self.__getAttrMethod('read', name)

            if write_addr is not None:
                wfun = self.__getAttrMethod('write', name)
            else:
                wfun = None
        self._prepareEvents(name, events)
        if IamChecker is not None:
            try:
                self.impl.setChecker(read_addr, IamChecker)
            except Exception as e:
                self.impl.error_stream(
                    "{0} cannot be added in the checker set due to:\n{1}"
                    "".format(name, e))
        if meanings is not None:
            if is_fresh_created:
                # FIXME: reestablish those parameters in the kwargs
                #  or they aren't well propagated
                if minValue is not None and 'minValue' not in kwargs:
                    kwargs['minValue'] = minValue
                if maxValue is not None and 'maxValue' not in kwargs:
                    kwargs['maxValue'] = maxValue
                return self._prepareAttrWithMeaning(
                    name, tango_T, meanings, qualities, rfun, wfun, **kwargs)
            else:
                self.impl._plcAttrs[name].meanings = meanings
        elif qualities is not None:
            if is_fresh_created:
                # FIXME: reestablish those parameters in the kwargs
                #  or they aren't well propagated
                if minValue is not None and 'minValue' not in kwargs:
                    kwargs['minValue'] = minValue
                if maxValue is not None and 'maxValue' not in kwargs:
                    kwargs['maxValue'] = maxValue
                return self._prepareAttrWithQualities(
                    name, tango_T, qualities, rfun, wfun, label=label, **kwargs)
            else:
                self.impl._plcAttrs[name].qualities = qualities
        else:
            if is_fresh_created:
                return self.add_Attr(
                    name, tango_T, rfun, wfun, minValue=minValue,
                    maxValue=maxValue, **kwargs)

    def add_AttrAddrBit(self, name, read_addr=None, read_bit=0,
                        write_addr=None, write_bit=None, meanings=None,
                        qualities=None, events=None, isRst=False,
                        activeRst_t=None, formula=None, switchDescriptor=None,
                        readback=None, setpoint=None, logLevel=None,
                        label=None, desc=None, minValue=None, maxValue=None,
                        *args, **kwargs):
        '''This method is a builder of a boolean dynamic attribute, even for RO
           than for RW. There are many optional parameters.

           With the meanings argument, moreover the DevBoolean a DevString
           attribute will be also generated (suffixed *_Status) with the same
           event and qualities configuration if they are, and will have a
           human readable message from the concatenation of the value and its
           meaning.

           There are also boolean attributes with a reset feature, those are
           attributes that can be triggered and after some short period of time
           they are automatically set back. The time with this reset active
           can be generic (and uses ACTIVE_RESET_T from the constants) or can
           be specified for a particular attribute using the activeRst_t.

           Another feature implemented for this type of attributes is the
           formula. That requires a dictionary with keys:
           + 'read' | 'write': they contain an string to be evaluated when
             value changes like a filter or to avoid an action based on some
             condition.
           For example, this is used to avoid to power up klystrons if there
           is an interlock, or to switch of the led when an interlock occurs.
           {'read':'VALUE and '\
                   'self._plcAttrs[\'HVPS_ST\'][\'read_value\'] == 9 and '\
                   'self._plcAttrs[\'Pulse_ST\'][\'read_value\'] == 8',
            'write':'VALUE and '\
                   'self._plcAttrs[\'HVPS_ST\'][\'read_value\'] == 8 and '\
                   'self._plcAttrs[\'Pulse_ST\'][\'read_value\'] == 7'
            },

           The latest feature implemented has relation with the rampeable
           attributes and this is a secondary configuration for the
           AttrRampeable DevDouble attributes, but in this case the feature
           to complain is to manage ramping on the booleans that power on and
           off those elements.
           The ramp itself shall be defined in the DevDouble attribute, the
           switch attribute only needs to know where to send this when state
           changes.
           The switchDescriptor is a dictionary with keys:
           + ATTR2RAMP: the name of the numerical attribute involved with the
             state transition.
           + WHENON | WHENOFF: keys to differentiate action interval between
             the two possible state changes.
             - FROM: initial value of the state change ramp
             - TO: final value of the state change ramp
             About those two last keys, they can be both or only one.
           + AUTOSTOP: in case it has also the autostop feature, this is used
             to identify the buffer to clean when transition from off to on.

           The set of arguments about readback, setpoint and switch are there
           to store defined relations between attributes. That is, to allow the
           setpoint (that has a read and write addresses) to know if there is
           another read only attribute that does the measure of what the
           setpoint sets. Also this readback may like to know about the
           setpoint and if the element is switch on or off.
        '''
        self.__traceAttrAddr(name, tango.DevBoolean, readAddr=read_addr,
                             readBit=read_bit, writeAddr=write_addr,
                             writeBit=write_bit)
        try:
            read_addr = self.__check_addresses_and_block_sizes(
                name, read_addr, write_addr)
        except IndexError:
            return
        is_fresh_created = self._prepareAttribute(
            name, tango.DevBoolean, readAddr=read_addr, readBit=read_bit,
            writeAddr=write_addr, writeBit=write_bit, formula=formula,
            readback=readback, setpoint=setpoint, label=label, description=desc,
            minValue=minValue, maxValue=maxValue,
            *args, **kwargs)
        if not is_fresh_created:
            return
        rfun = self.__getAttrMethod('read', name, isBit=True)

        if write_addr is not None:
            wfun = self.__getAttrMethod('write', name, isBit=True)
            if write_bit is None:
                write_bit = read_bit
        else:
            wfun = None
        if isRst:
            self.impl._plcAttrs[name][ISRESET] = True
            self.impl._plcAttrs[name][RESETTIME] = None
            if activeRst_t is not None:
                self.impl._plcAttrs[name][RESETACTIVE] = activeRst_t
        if type(switchDescriptor) == dict:
            self.impl._plcAttrs[name][SWITCHDESCRIPTOR] = switchDescriptor
            self.impl._plcAttrs[name][SWITCHDEST] = None
            # in the construction of the AutoStopAttr() the current switch
            # may not be build yet. Then now they must be linked together.
            if AUTOSTOP in switchDescriptor:
                autostopAttrName = switchDescriptor[AUTOSTOP]
                if autostopAttrName in self.impl._internalAttrs:
                    autostopper = self.impl._internalAttrs[autostopAttrName]
                    if autostopper.switch == name:
                        autostopper.setSwitchAttr(self.impl._plcAttrs[name])
        self._prepareEvents(name, events)
        if logLevel is not None:
            self.impl._getAttrStruct(name).logLevel = logLevel
        if meanings is not None:
            return self._prepareAttrWithMeaning(name, tango.DevBoolean,
                                                meanings, qualities, rfun,
                                                wfun, historyBuffer=None,
                                                **kwargs)
        else:
            return self.add_Attr(name, tango.DevBoolean, rfun, wfun,
                                 minValue=minValue, maxValue=maxValue,
                                 **kwargs)

    def add_AttrGrpBit(self, name, attrGroup=None, meanings=None,
                       qualities=None, events=None, **kwargs):
        """An special type of attribute where, given a set of bits by the pair
           [reg,bit] this attribute can operate all of them as one.
           That is, the read value is True if _all_ are true.
                    the write value, is applied to _all_ of them
                    (almost) at the same time.
        """
        self.__traceAttrAddr(name, tango.DevBoolean, internal=True)
        if name in self.impl._plcAttrs:
            raise AssertionError(
                "Trying to define an internal attribute {0} when there is a "
                "plc attribute already with the same name".format(name))
        if name not in self.impl._internalAttrs:
            attrObj = GroupAttr(name=name, device=self.impl, group=attrGroup)
            self.impl._internalAttrs[name] = attrObj
        else:
            attrObj = self.impl._internalAttr[name]
        rfun = attrObj.read_attr
        wfun = attrObj.write_attr
        toReturn = [self.add_Attr(name, tango.DevBoolean, rfun, wfun,
                                  **kwargs)]
        if qualities is not None:
            attrObj.qualities = qualities
        if meanings is not None:
            meaningAttr = self._buildMeaningAttr(attrObj, meanings, rfun,
                                                 **kwargs)
            toReturn.append(meaningAttr)
        self._prepareEvents(name, events)
        return tuple(toReturn)

    def add_AttrLogic(self, name, logic, label, desc, events=None,
                      operator='and', inverted=False, **kwargs):
        '''Internal type of attribute made to evaluate a logical formula with
           other attributes owned by the device with a boolean result.
        '''
        self.__traceAttrAddr(name, tango.DevBoolean, internalRO=True)
        self.impl.debug_stream("{0} logic: {1}".format(name, logic))
        # self._prepareInternalAttribute(name, tango.DevBoolean, logic=logic,
        #                                operator=operator, inverted=inverted)
        if name in self.impl._plcAttrs:
            raise AssertionError(
                "Trying to define an internal attribute {0} when there is a "
                "plc attribute already with the same name".format(name))
        if name not in self.impl._internalAttrs:
            is_fresh_created = True
            attrObj = LogicAttr(name=name, device=self.impl,
                                valueType=tango.DevBoolean,logic=logic,
                                operator=operator, inverted=inverted)
            self.impl._internalAttrs[name] = attrObj
        else:
            is_fresh_created = False
            self.impl.warn_stream("{0} already exist, overloading it!"
                                  "".format(name))
            attrObj = self.impl._internalAttrs[name]
            if not isinstance(attrObj, LogicAttr):
                raise AssertionError(
                    "Trying to convert {0} to a Logical attribute when was "
                    "NOT defined as such originally".format(name))
            if logic != attrObj._logic:
                self.impl.info_stream("\tmodify the logic from {0} to {1}"
                                      "".format(attrObj._logic, logic))
                self._logic = logic
            if operator != attrObj._operator:
                self.impl.info_stream("\tmodify the operator from {0} to {1}"
                                      "".format(attrObj._operator, operator))
                self._operator = operator
            if inverted != attrObj._inverted:
                self.impl.info_stream("\tmodify the inverted from {0} to {1}"
                                      "".format(attrObj._inverted, inverted))
                self._inverted = inverted
        rfun = self.__getAttrMethod('read', name, isLogical=True)
        wfun = None  # this kind can only be ReadOnly
        for key in logic:
            self.append2relations(name, LOGIC, key)
        self._prepareEvents(name, events)
        if is_fresh_created:
            return self.add_Attr(name, tango.DevBoolean, rfun, wfun, label,
                                 **kwargs)

    def add_AttrRampeable(self, name, T, read_addr, write_addr, label, unit,
                          rampsDescriptor, events=None, qualities=None,
                          readback=None, switch=None, desc=None, minValue=None,
                          maxValue=None, *args, **kwargs):
        '''Given 2 plc memory positions (for read and write), with this method
           build a RW attribute that looks like the other RWs but it includes
           ramping features.
           - rampsDescriptor is a dictionary with two main keys:
             + ASCENDING | DESCENDING: Each of these keys contain a
               dictionary in side describing the behaviour of the ramp
               ('+' mandatory keys, '-' optional keys):
               + STEP: value added/subtracted on each step.
               + STEPTIME: seconds until next step.
               - THRESHOLD: initial value from where start ramping.
               - SWITCH: attribute to monitor if it has switched off
           Those keys will generate attributes called '$name_$key' as memorised
           to allow the user to adapt the behaviour depending on configuration.

           About the threshold, it's a request from the user to have, it
           klystronHV, to not apply the ramp between 0 to N and after, if it's
           above, ramp it to the setpoint. Also the request of the user is to
           only do this ramp in the increasing way and decrease goes direct.
           Example:
           - rampsDescriptor = {ASCENDING:
                                   {STEP:0.5,#kV
                                    STEPTIME:1,#s
                                    THRESHOLD:20,#kV
                                    SWITCH:'HVPS_ONC'
                                   }}

           Another request for the Filament voltage is a descending ramp in
           similar characteristics than klystrons, but also: once commanded a
           power off, delay it doing a ramps to 0. This second request will
           be managed from the boolean that does this on/off transition using
           AttrAddrBit() builder together with a switchDescriptor dictionary.
           Example:
           - rampsDescriptor = {DESCENDING:
                                   {STEP:1,#kV
                                    STEPTIME:1,#s
                                    THRESHOLD:-50,#kV
                                    SWITCH:'GUN_HV_ONC'
                                   },
                                ASCENDING:
                                   {STEP:5,#kV
                                    STEPTIME:0.5,#s
                                    THRESHOLD:-90,#kV
                                    SWITCH:'GUN_HV_ONC'
                                   }}
           The set of arguments about readback, setpoint and switch are there
           to store defined relations between attributes. That is, to allow the
           setpoint (that has a read and write addresses) to know if there is
           another read only attribute that does the measure of what the
           setpoint sets. Also this readback may like to know about the
           setpoint and if the element is switch on or off.
        '''
        self.__traceAttrAddr(name, T, readAddr=read_addr, writeAddr=write_addr)
        tango_T = self.__mapTypes(T)
        is_fresh_created = self._prepareAttribute(
            name, T, readAddr=read_addr, writeAddr=write_addr,
            readback=readback, switch=switch, label=label, description=desc,
            minValue=minValue, maxValue=maxValue, *args, **kwargs)
        if not is_fresh_created:
            return
        rfun = self.__getAttrMethod('read', name)
        wfun = self.__getAttrMethod('write', name, rampeable=True)
        self._prepareEvents(name, events)
        if qualities is not None:
            rampeableAttr = self._prepareAttrWithQualities(name, tango_T,
                                                           qualities, rfun,
                                                           wfun, label=label,
                                                           **kwargs)
        else:
            rampeableAttr = self.add_Attr(name, tango_T, rfun, wfun, label,
                                          minValue=minValue, maxValue=maxValue,
                                          **kwargs)
        # until here, it's not different than another attribute
        # Next is specific for rampeable attributes
        rampAttributes = []
        rampAttributes.insert(0, rampeableAttr)
        return tuple(rampAttributes)

    def add_AttrPLC(self, heart, lockst, read_lockingAddr, read_lockingBit,
                    write_lockingAddr, write_lockingBit):
        heartbeat = self.add_AttrHeartBeat(heart)
        lockState, lockStatus = self.add_AttrLock_ST(lockst)
        locking = self.add_AttrLocking(read_lockingAddr, read_lockingBit,
                                       write_lockingAddr, write_lockingBit)
        return (heartbeat, lockState, lockStatus, locking)

    def add_AttrLock_ST(self, read_addr):
        COMM_STATUS = {0: 'unlocked', 1: 'local', 2: 'remote'}
        COMM_QUALITIES = {ALARM: [0], WARNING: [2]}
        plc_name = self.impl.get_name().split('/')[-1]
        desc = 'lock status {0}'.format(plc_name)
        # This attr was a number but for the user what shows the ----
        # information is an string
        self.impl.lock_ST = read_addr
        self.impl.setChecker(self.impl.lock_ST, ['\x00', '\x01', '\x02'])
        LockAttrs = self.add_AttrAddr(
            'Lock_ST', tango.DevUChar, read_addr, label=desc,
            desc=desc+john(COMM_STATUS), meanings=COMM_STATUS,
            qualities=COMM_QUALITIES, events={})
        # This UChar is to know what to read from the plc, the AttrAddr,
        # because it has an enumerate, will set this attr as string
        self.impl.set_change_event('Lock_ST', True, False)
        self.impl.set_change_event('Lock_Status', True, False)
        return LockAttrs

    def add_AttrLocking(self, read_addr, read_bit, write_addr, write_bit):
        desc = 'True when attempting to obtain write lock'
        new_attr = self.add_AttrAddrBit(
            'Locking', read_addr, read_bit, write_addr, write_bit, desc=desc,
            events={})
        locking_attr = self.impl.get_device_attr().get_attr_by_name('Locking')
        self.impl.Locking = locking_attr
        locking_attr.set_write_value(False)
        self.impl.locking_raddr = read_addr
        self.impl.locking_rbit = read_bit
        self.impl.locking_waddr = write_addr
        self.impl.locking_wbit = write_bit
        self.impl.set_change_event('Locking', True, False)
        return new_attr

    def add_AttrHeartBeat(self, read_addr, read_bit=0):
        self.impl.heartbeat_addr = read_addr
        desc = 'cadence bit going from True to False when PLC is okay'
        attr = self.add_AttrAddrBit('HeartBeat', read_addr, read_bit,
                                    desc=desc, events={})
        self.impl.set_change_event('HeartBeat', True, False)
        return attr

    def add_AttrEnumeration(self, name, prefix=None, suffixes=None,
                            *args, **kwargs):
        self.impl.info_stream("Building a Enumeration attribute set for {0}"
                              "".format(name))
        if prefix is not None:
            # With the klystrons user likes to see the number in the label,
            # we don't want in the attribute name because it will make
            # different between those two equal devices.
            try:
                plcN = int(self.impl.get_name().split('plc')[-1])
            except ValueError:
                plcN = 0
            if plcN in [4, 5]:
                label = "{0}{1:d}_{2}".format(prefix, plcN-3, name)
                name = "{0}_{1}".format(prefix, name)
            else:
                label = "{0}_{1}".format(prefix, name)
                name = "{0}_{1}".format(prefix, name)
            # FIXME: but this is something "ad hoc"
        else:
            label = "{0}".format(name)
        if suffixes is None:
            suffixes = {'options': [tango.DevString, 'read_write'],
                        'active': [tango.DevString, 'read_write'],
                        'numeric': [tango.DevShort, 'read_only'],
                        'meaning': [tango.DevString, 'read_only']}
        attrs = []
        try:
            if name in self.impl._plcAttrs:
                raise AssertionError(
                    "Trying to define an enumeration attribute {0} when there "
                    "is a plc attribute already with the same name"
                    "".format(name))
            if name not in self.impl._internalAttrs:
                enumObj = EnumerationAttr(name=name, label=label,
                                          device=self.impl,
                                          valueType=None, events={})
                for suffix in suffixes:
                    suffix_obj = getattr(enumObj, "_{0}".format(suffix))
                    data_type = suffixes[suffix][0]
                    if suffixes[suffix][1] == 'read_only':
                        memorised, writable = False, False
                    else:
                        memorised, writable = True, True
                    attrs.append(self._buildEnumerationParameter(
                        name, label, suffix, suffix_obj, data_type,
                        memorised=memorised, writable=writable))
                self.impl._internalAttrs[name] = enumObj
                enumObj.device = self.impl
            else:
                raise AssertionError(
                    "Trying to modify an existing Enumeration attribute {0}. "
                    "It is not allowed!".format(name))
        except Exception as e:
            self.impl.error_stream(
                "Fatal exception building {0}: {1}".format(name, e))
            traceback.print_exc()
        # No need to configure device memorised attributes because the
        # _LinacAttr subclasses already have this feature nested in the
        # implementation.
        return tuple(attrs)

    def _buildEnumerationParameter(self, baseName, baseLabel, suffix, parameterObj,
                                   dataType, memorised=False, writable=False):

        attrName = "{0}_{1}".format(baseName, suffix)
        parameterObj.alias = attrName
        if memorised:
            parameterObj.setMemorised()
        rfun = parameterObj.read_attr
        if writable:
            wfun = parameterObj.write_attr
        else:
            wfun = None
        self.impl._internalAttrs[attrName] = parameterObj
        return self.add_Attr(attrName, dataType,
                             rfun=rfun, wfun=wfun, label=baseLabel)



    def remove_all(self):
        for attr in self.alist:
            try:
                self.impl.remove_attribute(attr.get_name())
            except ValueError as exc:
                self.impl.debug_stream("{0}: {1}".format(
                    attr.get_name(), str(exc)))

    def build(self, fname):
        if self._builder is not None:
            if not isinstance(self._builder, threading.Thread):
                msg = "AttrList builder is not a Thread! (%s)" \
                      % (type(self._builder))
                self.impl.error_stream("Ups! This should never happen: {0}"
                                       "".format(msg))
                raise AssertionError(msg)
            elif self._builder.isAlive():
                msg = "AttrList build while it is building"
                self.impl.error_stream("Ups! This should never happen: {0}"
                                       "".format(msg))
                return
            else:
                self._builder = None
        self._builder = threading.Thread(name="FileParser",
                                        target=self.parse_file, args=(fname,))
        self.impl.info_stream("Launch a thread to build the dynamic attrs")
        self._builder.start()

    def parse_file(self,  fname):
        t0 = time.time()
        self._fileParsed.clear()
        msg = "{0:30s}\t{1:10s}\t{2:5s}\t{3:6s}\t{4:6s}" \
              "".format("'attrName'", "'Type'", "'RO/RW'", "'read'", "'write'")
        self.impl.info_stream(msg)
        try:
            # FIXME: pending to know that it also works fine
            #  in suse11.1 (python 2.6)
            try:
                self.impl.debug_stream("__file__ {0!r}".format(__file__))
                fullpath = path.dirname(__file__)
                self.impl.debug_stream("fullpath {0!r}".format(fullpath))
                if len(fullpath) == 0:
                    fullpath = '.'
            except Exception:
                fullpath = path.realpath('.')
                self.impl.debug_stream("fullpath {0!r}".format(fullpath))
            fullname = "{0}/{1}".format(fullpath, fname)
            self.impl.debug_stream("fullname {0!r}".format(fullname))
            execfile(fullname, self.globals_, self.locals_)
        except IOError as io:
            self.impl.error_stream("AttrList.parse_file IOError: {0}\n{1}"
                                   "".format(io, traceback.format_exc()))
            raise LinacException(io)
        except Exception as exc:
            self.impl.error_stream("AttrList.parse_file Exception: {0}\n{1}"
                                   "".format(exc, traceback.format_exc()))
        self.impl.debug_stream('Parse attrFile done.')
        # Here, I can be sure that all the objects are build,
        # then any none existing object reports a configuration
        # mistake in the parsed file.
        for origName in self._relations:
            try:
                origObj = self.impl._getAttrStruct(origName)
                for tag in self._relations[origName]:
                    for destName in self._relations[origName][tag]:
                        try:
                            destObj = self.impl._getAttrStruct(destName)
                            origObj.addReportTo(destObj)
                            self.impl.debug_stream(
                                "Linking {0} with {1} ({2})"
                                "".format(origName, destName, tag))
                            origObj.reporter.report()
                        except Exception as e:
                            self.impl.error_stream(
                                "Exception managing the relation between {0}"
                                " and {1}: {2}".format(origName, destName, e))
            except Exception as e:
                self.impl.error_stream("Exception managing {0} relations: {1}"
                                       "".format(origName, e))
                traceback.print_exc()
        self.impl.applyCheckers()
        tf = time.time()
        self.impl.info_stream(
            "file parsed in {0:6.3f} seconds".format(tf - t0))
        t0 = time.time()
        self.impl.info_stream("check if there are more attributes defined "
                              "in the DynamicAttributes property")
        self.impl.updateDynamicAttributes()  # check if there are more attrs
        tf = time.time()
        self.impl.info_stream(
            "DynamicAttributes parsed in {0:6.3f} seconds".format(tf - t0))
        self._fileParsed.set()

    def parse(self, text):
        exec(text, self.globals_, self.locals_)

    # # internal auxiliar methods ---

    def __getAttrMethod(self, operation, attrName, isBit=False,
                        rampeable=False, internal=False, isGroup=False,
                        isLogical=False):
        # if exist an specific method
        attrStruct = self.impl._getAttrStruct(attrName)
        return getattr(attrStruct, "%s_attr" % (operation))

    def __traceAttrAddr(self, attrName, attrType, readAddr=None, readBit=None,
                        writeAddr=None, writeBit=None, internal=False,
                        internalRO=False):
        # first column is the attrName
        msg = "{0:30s}\t".format("'{0}'".format(attrName))
        # second, its type
        msg += "{0:10s}\t".format("'{0}'".format(attrType))
        # Then, if it's read only or read/write
        if writeAddr is not None or internal:
            msg += "   'RW'\t"
        else:
            msg += "'RO'   \t"
        if readAddr is not None:
            if readBit is not None:
                read = "'{0}.{1}'".format(readAddr, readBit)
            else:
                read = "'{0}'".format(readAddr)
            msg += "{0:6s}\t".format(read)
        if writeAddr is not None:
            if writeBit is not None:
                write = "'{0}.{1}'".format(writeAddr, writeBit)
            else:
                write = "'{0}'".format(writeAddr)
            msg += "{0:6s}\t".format(write)
        self.impl.info_stream(msg)

    # # prepare attribute structures ---

    def _prepareAttribute(self, attrName, attrType, readAddr, readBit=None,
                          writeAddr=None, writeBit=None, formula=None,
                          readback=None, setpoint=None, switch=None,
                          label=None, description=None,
                          minValue=None, maxValue=None, **kwargs):
        """
This is a constructor of the item in the dictionary of attributes related
with PLC memory locations. At least they have a read address and a type. The
booleans also needs a read bit. For writable attributes there are the
equivalents write addresses and booleans also the write bit (it doesn't
happen with current PLCs, but we support them if different).

Also is introduced the feature of the formula that can distinguish between a
read formula and write case. Not needing both coexisting.

The set of arguments about readback, setpoint and switch are there to store
defined relations between attributes. That is, to allow the setpoint (that
has a read and write addresses) to know if there is another read only
attribute that does the measure of what the setpoint sets. Also this readback
may like to know about the setpoint and if the element is switch on or off.

        :param attrName:
        :param valueType:
        :param readAddr:
        :param readBit:
        :param writeAddr:
        :param writeBit:
        :param formula:
        :param readback:
        :param setpoint:
        :param switch:
        :param label:
        :param description
        :param minValue:
        :param maxValue:
        :return: fresh created boolean
        """
        if readAddr is None and writeAddr is not None:
            readAddr = self._db20_size + writeAddr
        if attrName in self.impl._internalAttrs:
            raise AssertionError(
                "Trying to define a plc attribute {0} when there is an "
                "internal attribute already with the same name"
                "".format(attrName))
        if attrName not in self.impl._plcAttrs:
            is_fresh_created = True
            attrObj = PLCAttr(name=attrName, device=self.impl,
                              valueType=attrType,
                              readAddr=readAddr, readBit=readBit,
                              writeAddr=writeAddr, writeBit=writeBit,
                              formula=formula,
                              readback=readback, setpoint=setpoint,
                              switch=switch,
                              label=label, description=description,
                              minValue=minValue, maxValue=maxValue)
            self.impl._plcAttrs[attrName] = attrObj
        else:
            is_fresh_created = False
            self.impl.warn_stream("{0} already exist, overloading it!"
                                  "".format(attrName))
            attrObj = self.impl._plcAttrs[attrName]
            if not isinstance(attrObj, PLCAttr):
                raise AssertionError(
                    "Trying to convert {0} to a PLCAttr attribute "
                    "when was NOT defined as such originally".format(attrName))
            # TODO: protection against datatype change
            if readAddr is not None and readAddr != attrObj.read_addr:
                self.impl.info_stream("\tmodify the read_addr from {0} to {1}"
                                      "".format(attrObj.read_addr, readAddr))
                attrObj._readAddr = readAddr
            if readBit is not None and readBit != attrObj.read_bit:
                self.impl.info_stream("\tmodify the read_bit from {0} to {1}"
                                      "".format(attrObj.read_bit, readBit))
                attrObj._readBit = readBit
            if writeAddr is not None and writeAddr != attrObj.write_addr:
                self.impl.info_stream("\tmodify the write_addr from {0} to {1}"
                                      "".format(attrObj.write_addr, writeAddr))
                attrObj._writeAddr = writeAddr
            if writeBit is not None and writeBit != attrObj.write_bit:
                self.impl.info_stream("\tmodify the write_bit from {0} to {1}"
                                      "".format(attrObj.write_bit, writeBit))
                attrObj._writeBit = writeBit
            if formula is not None and formula != attrObj.formula:
                self.impl.info_stream("\tmodify the formula from {0} to {1}"
                                      "".format(attrObj.formula, formula))
                attrObj.setFormula(formula)
            if readback is not None or setpoint is not None or \
                    switch is not None:
                raise AssertionError(
                    "Not yet supported the feature for readback, setpoints "
                    "and switches")
            if any([label is not None, description is not None,
                    minValue is not None, maxValue is not None]):
                self.impl.info_stream("label {0}".format(label))
                self.impl.info_stream("description {0}".format(description))
                self.impl.info_stream("minValue {0}".format(minValue))
                self.impl.info_stream("maxValue {0}".format(maxValue))
                multiattribute_obj = self.impl.get_device_attr()
                attribute_obj = multiattribute_obj.get_attr_by_name(attrName)
                properties_obj = attribute_obj.get_properties()
                self.impl.info_stream(
                    "\t>> get the attribute properties object")
                if label is not None and label != attrObj.label:
                    self.impl.info_stream(
                        "\t+ modify the label from {0} to {1}"
                        "".format(attrObj.label, label))
                    attrObj._label = label
                    properties_obj.label = label
                if description is not None and \
                        description != attrObj.description:
                    self.impl.info_stream(
                        "\t+ modify the description from {0} to {1}"
                        "".format(attrObj.description, description))
                    attrObj._description = description
                    properties_obj.description = description
                if minValue is not None and minValue != attrObj.minValue:
                    self.impl.info_stream(
                        "\t+ modify the minValue from {0} to {1}"
                        "".format(attrObj.minValue, minValue))
                    attrObj._minValue = minValue
                    if tango.__version_info__[0] == 7:
                        self.impl.info_stream("convert the value to string")
                        properties_obj.min_value = str(minValue)
                    else:
                        properties_obj.min_value = minValue
                if maxValue is not None and maxValue != attrObj.maxValue:
                    self.impl.info_stream(
                        "\t+ modify the maxValue from {0} to {1}"
                        "".format(attrObj.maxValue, maxValue))
                    attrObj._maxValue = maxValue
                    if tango.__version_info__[0] == 7:
                        self.impl.info_stream("convert the value to string")
                        properties_obj.max_value = str(maxValue)
                    else:
                        properties_obj.max_value = maxValue
                self.impl.info_stream(
                    "\t<< apply the attribute properties changes")
                try:
                    if tango.__version_info__[0] == 7:
                        self.impl.info_stream("old call to set_properties")
                        attribute_obj.set_properties(properties_obj, self.impl)
                    else:
                        attribute_obj.set_properties(properties_obj)
                    self.impl.info_stream(
                        "changed the properties of {0}:\n"
                        "properties object.".format(attrName))
                except TypeError as exc:
                    self.impl.error_stream(
                        "error setting properties to {0}:\n"
                        "properties object: {1}\nException: {2}".format(
                            attrName, properties_obj, exc))
        self._insert2reverseDictionary(attrName, attrType, readAddr, readBit,
                                       writeAddr, writeBit)
        return is_fresh_created

    def _insert2reverseDictionary(self, name, valueType, readAddr, readBit,
                                  writeAddr, writeBit):
        '''
        Hackish for the dump of a valid write block when the PLC provides
        invalid values in the write datablock

        :param name:
        :param valueType:
        :param readAddr:
        :param readBit:
        :param writeAddr:
        :param writeBit:
        :return:
        '''
        dct = self.impl._addrDct
        if 'readBlock' not in dct:
            dct['readBlock'] = {}
        rDct = dct['readBlock']
        if 'writeBlock' not in dct:
            dct['writeBlock'] = {}
        wDct = dct['writeBlock']
        if readBit is not None:  # boolean
            if readAddr not in rDct:
                rDct[readAddr] = {}
            if readBit in rDct[readAddr]:
                self.impl.warn_stream(
                    "{0} override readAddr {1} readBit {2}: {3}"
                    "".format(name, readAddr, readBit,
                              rDct[readAddr][readBit]['name']))
            rDct[readAddr][readBit] = {}
            rDct[readAddr][readBit]['name'] = name
            rDct[readAddr][readBit]['type'] = valueType
            if writeAddr is not None:
                rDct[readAddr][readBit]['writeAddr'] = writeAddr
                if writeBit is None:
                    writeBit = readBit
                rDct[readAddr][readBit]['writeBit'] = writeBit
                if writeAddr not in wDct:
                    wDct[writeAddr] = {}
                wDct[writeAddr][writeBit] = {}
                wDct[writeAddr][writeBit]['name'] = name
                wDct[writeAddr][writeBit]['type'] = valueType
                wDct[writeAddr][writeBit]['readAddr'] = readAddr
                wDct[writeAddr][writeBit]['readBit'] = readBit
        else:  # Byte, Word or Float
            if readAddr in rDct:
                self.impl.warn_stream(
                    "{0} override readAddr {1}: {2}"
                    "".format(name, readAddr, rDct[readAddr]['name']))
            rDct[readAddr] = {}
            rDct[readAddr]['name'] = name
            rDct[readAddr]['type'] = valueType
            if writeAddr is not None:
                rDct[readAddr]['writeAddr'] = writeAddr
                if writeAddr in wDct:
                    self.impl.warn_stream(
                        "{0} override writeAddr {1}:{2}"
                        "".format(name, writeAddr, wDct[writeAddr]['name']))
                wDct[writeAddr] = {}
                wDct[writeAddr]['name'] = name
                wDct[writeAddr]['type'] = valueType
                wDct[writeAddr]['readAddr'] = readAddr

    def _prepareEvents(self, attrName, eventConfig):
        if eventConfig is not None:
            attrStruct = self.impl._getAttrStruct(attrName)
            attrStruct[EVENTS] = eventConfig
            attrStruct[LASTEVENTQUALITY] = tango.AttrQuality.ATTR_VALID
            attrStruct[EVENTTIME] = None

    def _prepareAttrWithMeaning(self, attrName, attrType, meanings, qualities,
                                rfun, wfun, historyBuffer=None, **kwargs):
        '''There are some short integers where the number doesn't mean anything
           by itself. The plcs register description shows a relation between
           the possible numbers and its meaning.
           These attributes are splitted in two:
           - one with only the number (machine readable: archiver,plots)
           - another string with the number and its meaning (human readable)

           The historyBuffer parameter has been developed to introduce
           interlock tracking (using a secondary attribute called *_History).
           That is, starting from a set of non interlock values, when the
           attibute reads something different to them, it starts collecting
           those new values in order to provide a line in the interlock
           activity. Until the interlock is cleaned, read value is again in
           the list of non interlock values and this buffer is cleaned.
        '''
        # first, build the same than has been archived
        attrState = self.add_Attr(attrName, attrType, rfun, wfun, **kwargs)
        # then prepare the human readable attribute as a feature
        attrStruct = self.impl._plcAttrs[attrName]
        attrStruct.qualities = qualities
        attrTuple = self._buildMeaningAttr(attrStruct, meanings, rfun,
                                             **kwargs)
        toReturn = (attrState,)
        toReturn += (attrTuple,)
        return toReturn

    def _buildMeaningAttr(self, attrObj, meanings, rfun, historyBuffer=None,
                          **kwargs):
        if attrObj.name.endswith('_ST'):
            name = attrObj.name.replace('_ST', '_Status')
        else:
            name = "%s_Status" % (attrObj.name)
        attrObj.meanings = meanings
        self.impl._plcAttrs[name] = attrObj._meaningsObj
        self.impl._plcAttrs[name].alias = name
        self.impl._plcAttrs[name].meanings = meanings
        self.impl._plcAttrs[name].qualities = attrObj.qualities
        meaningAttr = self.add_Attr(name, tango.DevString, rfun, wfun=None,
                                    **kwargs)
        toReturn = (meaningAttr)
        if historyBuffer is not None:
            attrHistoryName = "%s_History" % (attrStruct._meaningsalias)
            attrStruct.history = historyBuffer
            historyStruct = attrStruct._historyObj
            historyStruct.history = historyBuffer
            historyStruct.alias = attrHistoryName
            attrStruct.read_value = HistoryBuffer(
                cleaners=historyBuffer[BASESET], maxlen=HISTORYLENGTH,
                owner=attrStruct)
            xdim = attrStruct.read_value.maxSize()
            self.impl._plcAttrs[attrHistoryName] = historyStruct
            attrHistory = self.add_Attr(attrHistoryName, tango.DevString,
                                        rfun=historyStruct.read_attr,
                                        xdim=xdim, **kwargs)
            toReturn += (attrHistory,)
        return toReturn

    def _prepareAttrWithQualities(self, attrName, attrType, qualities,
                                  rfun, wfun, label=None, unit=None,
                                  autoStop=None, **kwargs):
        '''The attributes with qualities definition, but without meanings for
           their possible values, are specifically build to have a
           CircularBuffer as the read element. That is made to collect a small
           record of the previous values, needed for the RELATIVE condition
           (mainly used with CHANGING quality). Without a bit of memory in the
           past is not possible to know what had happen.

           This kind of attributes have another possible keyword named
           'autoStop'. This has been initially made for the eGun HV
           leakage current, to stop it when this leak is too persistent on
           time (adjustable using an extra attribute). Apart from that, the
           user has a feature disable for it.

           TODO: feature 'too far' from a setpoint value.
        '''
        self.impl._plcAttrs[attrName][READVALUE] = \
            CircularBuffer([], owner=self.impl._plcAttrs[attrName])
        self.impl._plcAttrs[attrName][QUALITIES] = qualities
        toReturn = (self.add_Attr(attrName, attrType, rfun, wfun, label=label,
                                  unit=unit, **kwargs),)
        if autoStop is not None:
            # FIXME: shall it be in the AttrWithQualities? Or more generic?
            toReturn += self._buildAutoStopAttributes(attrName, label,
                                                      attrType, autoStop,
                                                      **kwargs)
        return toReturn

    # # Builders for subattributes ---

    def _buildAutoStopAttributes(self, baseName, baseLabel, attrType,
                                 autoStopDesc, logLevel, **kwargs):
        # TODO: review if the callback between attributes can be usefull here
        attrs = []
        autostopperName = "%s_%s" % (baseName, AUTOSTOP)
        autostopperLabel = "%s %s" % (baseLabel, AUTOSTOP)
        autostopSwitch = autoStopDesc.get(SWITCHDESCRIPTOR, None)
        if autostopSwitch in self.impl._plcAttrs:
            autostopSwitch = self.impl._plcAttrs[autostopSwitch]
            # depending on the build process, the switch object may not be
            # build yet. That's why the name (as string) is stored.
            # Later, when the switch (AttrAddrBit) is build, this assignment
            # will be completed.
        if autostopperName in self.impl._plcAttrs:
            raise AssertionError(
                "Trying to define an AutoStop attribute {0} when there is a "
                "plc attribute already with the same name"
                "".format(autostopperName))
        if autostopperName not in self.impl._internalAttrs:
            autostopper = AutoStopAttr(name=autostopperName,
                                       valueType=attrType,
                                       device=self.impl,
                                       plcAttr=self.impl._plcAttrs[baseName],
                                       below=autoStopDesc.get(BELOW, None),
                                       above=autoStopDesc.get(ABOVE, None),
                                       switchAttr=autostopSwitch,
                                       integr_t=autoStopDesc.get(INTEGRATIONTIME,
                                                                 None),
                                       events={})
            self.impl._internalAttrs[autostopperName] = autostopper
        else:
            raise AssertionError("Trying to modify an existing AutoStop "
                                 "attribute {0}. It is not allowed!"
                                 "".format(name))
        spectrumAttr = self.add_Attr(autostopperName, tango.DevDouble,
                                     rfun=autostopper.read_attr, xdim=1000,
                                     label=autostopperLabel)
        attrs.append(spectrumAttr)
        enableAttr = self._buildAutoStopperAttr(autostopperName,
                                                autostopperLabel, ENABLE,
                                                autostopper._enable,
                                                tango.DevBoolean,
                                                memorised=True, writable=True)
        attrs.append(enableAttr)
        for condition in [BELOW, ABOVE]:
            if condition in autoStopDesc:
                condAttr = self._buildAutoStopConditionAttr(condition,
                                                            autostopperName,
                                                            autostopperLabel,
                                                            autostopper)
                attrs.append(condAttr)
        integrAttr = self._buildAutoStopperAttr(autostopperName,
                                                autostopperLabel,
                                                INTEGRATIONTIME,
                                                autostopper._integr_t,
                                                tango.DevDouble,
                                                memorised=True, writable=True)
        meanAttr = self._buildAutoStopperAttr(autostopperName,
                                              autostopperLabel, MEAN,
                                              autostopper._mean,
                                              tango.DevDouble)
        attrs.append(meanAttr)
        stdAttr = self._buildAutoStopperAttr(autostopperName,
                                             autostopperLabel, STD,
                                             autostopper._std,
                                             tango.DevDouble)
        attrs.append(stdAttr)
        triggeredAttr = self._buildAutoStopperAttr(autostopperName,
                                                   autostopperLabel, TRIGGERED,
                                                   autostopper._triggered,
                                                   tango.DevBoolean)
        attrs.append(triggeredAttr)
        if logLevel is not None:
            autostopper.logLevel = logLevel
            # it is only necessary to set it in one of them (here is the main
            # one), but can be any because they share their logLevel.
        return tuple(attrs)

    def _buildAutoStopperAttr(self, baseName, baseLabel, suffix,
                              autostopperComponent, dataType, memorised=False,
                              writable=False):
        attrName = "{0}_{1}".format(baseName, suffix)
        attrLabel = "{0} {1}".format(baseLabel, suffix)
        autostopperComponent.alias = attrName
        if memorised:
            autostopperComponent.setMemorised()
        rfun = autostopperComponent.read_attr
        if writable:
            wfun = autostopperComponent.write_attr
        else:
            wfun = None
        self.impl._internalAttrs[attrName] = autostopperComponent
        return self.add_Attr(attrName, dataType,
                             rfun=rfun, wfun=wfun,
                             label=attrLabel)

    def _buildAutoStopConditionAttr(self, condition, baseName, baseLabel,
                                    autostopper):
        conditionName = "{0}_{1}_Threshold".format(baseName, condition)
        conditionLabel = "{0} %{1} Threshold".format(baseName, condition)
        conditioner = getattr(autostopper, '_{0}'.format(condition.lower()))
        conditioner.alias = conditionName
        conditioner.setMemorised()
        self.impl._internalAttrs[conditionName] = conditioner
        return self.add_Attr(conditionName, tango.DevDouble,
                             rfun=conditioner.read_attr,
                             wfun=conditioner.write_attr,
                             label=conditionLabel)

    def append2relations(self, origin, tag, dependency):
        self.impl.debug_stream(
            "{0} depends on {1} ({2})".format(origin, dependency, tag))
        if dependency not in self._relations:
            self._relations[dependency] = {}
        if tag not in self._relations[dependency]:
            self._relations[dependency][tag] = []
        self._relations[dependency][tag].append(origin)

    def __check_addresses_and_block_sizes(self, name, read_addr, write_addr):
        if read_addr is None and write_addr is not None:
            read_addr = self._db20_size+write_addr
            self.impl.debug_stream(
                "{0} define the read_addr {1} relative to the db20 size {2} "
                "and the write_addr{3}".format(name, read_addr,
                                               self._db20_size, write_addr))
        if read_addr > self.impl.ReadSize:
            self.impl.warn_stream(
                "{0} defines a read_addr {1} out of the size of the "
                "db20+db22 {2}: it will not be build"
                "".format(name, read_addr, self.impl.ReadSize))
            raise IndexError("Out of the DB20")
        if write_addr is not None and write_addr > self._db22_size:
            self.impl.warn_stream(
                "{0} defines a write_addr {1} out of the size of the db22 {2}: "
                "it will not be build".format(name, write_addr,
                                              self._db22_size))
            raise IndexError("Out of the DB22")
        return read_addr


def get_ip(iface):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sockfd = sock.fileno()
    SIOCGIFADDR = 0x8915
    ifreq = struct.pack('16sH14s', iface, socket.AF_INET, '\x00'*14)
    res = fcntl.ioctl(sockfd, SIOCGIFADDR, ifreq)
    ip = struct.unpack('16sH2x4s8x', res)[2]
    return socket.inet_ntoa(ip)

# PROTECTED REGION END --- linacdata.additionnal_import

# # Device States Description
# # INIT : The device is being initialised.
# # ON : PLC communication normal
# # ALARM : Transient issue
# # FAULT : Unrecoverable issue
# # UNKNOWN : No connection with the PLC, no state information


class LinacData(tango.Device_4Impl):

        # --------- Add you global variables here --------------------------
        # PROTECTED REGION ID(linacdata.global_variables) ---
        ReadSize = None
        WriteSize = None
        BindAddress = None  # deprecated
        LocalAddress = None
        RemoteAddress = None
        IpAddress = None  # deprecated
        PlcAddress = None
        Port = None
        LocalPort = None
        RemotePort = None
        # assigned by addAttrLocking
        locking_raddr = None
        locking_rbit = None
        locking_waddr = None
        locking_wbit = None
        lock_ST = None
        Locking = None
        is_lockedByTango = None
        heartbeat_addr = None
        AttrFile = None
        _plcAttrs = {}
        _internalAttrs = {}
        _addrDct = {}

        disconnect_t = 0
        read_db = None
        dataBlockSemaphore = threading.Semaphore()
        _important_logs = []

        _traceTooClose = []

        _prevMemDump = None
        _prevLockSt = None

        def debug_stream(self, msg):
            if sys.version_info[0:2] == (2, 6):
                super(LinacData, self).debug_stream(
                    "[%s] %s" % (threading.current_thread().getName(), msg))
            else:
                super(LinacData, self).debug_stream(
                    "[%s] %s", threading.current_thread().getName(), msg)

        def info_stream(self, msg):
            if sys.version_info[0:2] == (2, 6):
                super(LinacData, self).info_stream(
                    "[%s] %s" % (threading.current_thread().getName(), msg))
            else:
                super(LinacData, self).info_stream(
                    "[%s] %s", threading.current_thread().getName(), msg)

        def warn_stream(self, msg):
            if sys.version_info[0:2] == (2, 6):
                super(LinacData, self).warn_stream(
                    "[%s] %s" % (threading.current_thread().getName(), msg))
            else:
                super(LinacData, self).warn_stream(
                    "[%s] %s", threading.current_thread().getName(), msg)

        def error_stream(self, msg):
            if sys.version_info[0:2] == (2, 6):
                super(LinacData, self).error_stream(
                    "[%s] %s" % (threading.current_thread().getName(), msg))
            else:
                super(LinacData, self).error_stream(
                    "[%s] %s", threading.current_thread().getName(), msg)

        ####
        # PLC connectivity area ---
        def connect(self):
            '''This method is used to build the object that maintain the
               communications with the assigned PLC.
            '''
            if self.read_db is not None:
                return
            self.info_stream('connecting...')
            self.set_status('connecting...')
            try:
                self.__check_ports()
                self.read_db = tcpblock.open_datablock(self.PlcAddress,
                                                       self.Port,
                                                       self.ReadSize,
                                                       self.WriteSize,
                                                       self.BindAddress,
                                                       self.info_stream,
                                                       self.debug_stream,
                                                       self.warn_stream,
                                                       self.error_stream,
                                                       self.lock_ST)
                self.info_stream("build the tcpblock, socket {0:d}"
                                 "".format(self.read_db.sock.fileno()))
                self.write_db = self.read_db
                self.info_stream('connected')
                self.set_state(tango.DevState.ON)
                self.set_status('connected')
                self.applyCheckers()
                return True
            except Exception as e:
                self.error_stream('connection failed exception: {0}'
                                  ''.format(traceback.format_exc()))
                self.set_state(tango.DevState.FAULT)
                self.set_status("{0}".format(traceback.format_exc()))
                return False

        def disconnect(self):
            '''This method closes the connection to the assigned PLC.
            '''
            self.info_stream('disconnecting...')
            self.set_status('disconnecting...')
            # self._plcUpdatePeriod = PLC_MAX_UPDATE_PERIOD
            self._setPlcUpdatePeriod(PLC_MAX_UPDATE_PERIOD)
            try:
                if self.is_connected():
                    tcpblock.close_datablock(self.read_db, self.warn_stream)
                    self.read_db = None
                if self.get_state() == tango.DevState.ON:
                    self.set_state(tango.DevState.OFF)
                    self.set_status('not connected')
                return True
            except Exception:
                return False

        def reconnect(self):
            '''
            '''
            if time.time() - self.last_update_time > self.ReconnectWait:
                self.connect()

        def is_connected(self):
            '''Checks if the object that interfaces the communication with
               the PLC is well made and available.
            '''
            return self.read_db is not None and self.read_db.sock is not None

        def has_data_available(self):
            '''Check if there is some usable data give from the PLC.
            '''
            return self.is_connected() and \
                len(self.read_db.buf) == self.ReadSize

        def setChecker(self, addr, values):
            if not hasattr(self, '_checks'):
                self.debug_stream("Initialise checks dict")
                self._checks = {}
            if isinstance(addr, int) and isinstance(values, list):
                self.debug_stream("Adding a checker for address {0:d} "
                                 "with values {1}".format(addr, values))
                self._checks[addr] = values
                return True
            return False

        def applyCheckers(self):
            if hasattr(self, '_checks') and isinstance(self._checks, dict) and \
                    hasattr(self, 'read_db') and isinstance(self.read_db,
                                                            tcpblock.Datablock):
                try:
                    had = len(self.read_db._checks.keys())
                    for addr in self._checks:
                        if not addr in self.read_db._checks:
                            self.debug_stream(
                                "\tfor addr {0:d} insert {1}"
                                "".format(addr, self._checks[addr]))
                            self.read_db.setChecker(addr, self._checks[addr])
                        else:
                            lst = self.read_db.getChecker(addr)
                            for value in self._checks[addr]:
                                if value not in lst:
                                    self.debug_stream(
                                        "\tin addr  {0:d} append {1}"
                                        "".format(addr, self._checks[addr]))
                                    self.read_db._checks.append(value)
                    now = len(self.read_db._checks.keys())
                    if had != now:
                        self.debug_stream("From {0:d} to {1:d} checkers"
                                          "".format(had, now))
                except Exception as e:
                    self.error_stream("Incomplete applyCheckers: {0}"
                                      "".format(e))

        def forceWriteAttrs(self):
            '''There are certain situations, like the PLC shutdown, that
               results in a bad DB20 values received. Then the writable values
               cannot be written because the datablock only changes one
               register when many have bad values and is rejected by the plc.

               Due to this we force a construction of a complete write
               datablock to be send once for all.
            '''
            if not hasattr(self, 'write_db') or self.write_db is None:
                return
            wDct = self._addrDct['writeBlock']
            self.info_stream("Force to reconstruct the write data block")
            self.dataBlockSemaphore.acquire()
            self.attr_forceWriteDB_read = "%s\n" % (time.strftime(
                "%Y/%m/%d %H:%M:%S", time.localtime()))
            wblock_was = self.write_db.buf[self.write_db.write_start:]
            try:
                for wAddr in wDct:
                    if 'readAddr' in wDct[wAddr]:  # Uchars, Shorts, floats
                        name = wDct[wAddr]['name']
                        rAddr = wDct[wAddr]['readAddr']
                        T, size = TYPE_MAP[wDct[wAddr]['type']]
                        rValue = self.read_db.get(rAddr, T, size)
                        wValue = self.write_db.get(
                            wAddr+self.write_db.write_start, T, size)
                        msg = "{0} = ({1}, {2}) [{3} -> {4}, {5}, {6}]" \
                              "".format(name, rValue, wValue, rAddr, wAddr, T,
                                        size)
                        self.attr_forceWriteDB_read += "{0}\n".format(msg)
                        self.info_stream(msg)
                        self.write_db.write(wAddr, rValue, (T, size),
                                            dry=True)
                    else:  # booleans
                        was = byte = self.write_db.b(
                            wAddr+self.write_db.write_start)
                        msg = "booleans {0:d}".format(wAddr)
                        self.attr_forceWriteDB_read += "{0}\n".format(msg)
                        self.info_stream(msg)
                        for wBit in wDct[wAddr]:
                            name = wDct[wAddr][wBit]['name']
                            rAddr = wDct[wAddr][wBit]['readAddr']
                            rBit = wDct[wAddr][wBit]['readBit']
                            rValue = self.read_db.bit(rAddr, rBit)
                            wValue = self.write_db.bit(
                                wAddr+self.write_db.write_start, rBit)
                            msg = "\t{0} = ({1}, {2}) [{3}.{4} -> {5}.{6}]" \
                                  "".format(name, rValue, wValue, rAddr, rBit,
                                            wAddr, wBit)
                            self.attr_forceWriteDB_read += "{0}\n".format(msg)
                            self.info_stream(msg)
                            if rValue is True:
                                byte = byte | (int(1) << wBit)
                            else:
                                byte = byte & ((0xFF) ^ (1 << wBit))
                        msg = "{0:d} = {1} -> {2}".format(wAddr,
                                                          binaryByte(was),
                                                          binaryByte(byte))
                        self.attr_forceWriteDB_read += "{0}\n".format(msg)
                        self.info_stream(msg)
                self.write_db.rewrite()
                wblock_is = self.write_db.buf[self.write_db.write_start:]
                i = 0
                msg = "writeblock:\n{0:>11s}\t{1:>11s}\n".format("was:","now:")
                while i < len(wblock_was):
                    line = "{0:>11s}\t{1:>11s}\n" \
                           "".format(
                        ' '.join("{0:02x}".format(
                            x for x in wblock_was[i:i+4])),
                        ' '.join("{0:02x}".format(
                            x for x in wblock_is[i:i + 4])))
                    msg += line
                    i += 4
                self.attr_forceWriteDB_read += "{0}\n".format(msg)
                self.info_stream(msg)
            except Exception as e:
                msg = "Could not complete the force Write\n{0}".format(e)
                self.attr_forceWriteDB_read += "{0}\n".format(msg)
                self.error_stream(msg)
            self.dataBlockSemaphore.release()

        def __check_ports(self):
            if self.Port is None:
                if self.LocalPort is not None and self._deviceIsInLocal:
                    self.warn_stream("Establishing Port as LocalPort")
                    self.Port = self.LocalPort
                elif self.RemotePort is not None and self._deviceIsInRemote:
                    self.warn_stream("Establishing Port as RemotePort")
                    self.Port = self.RemotePort
        # Done PLC connectivity area ---

        ####
        # state/status manager methods ---
        def set_state(self, newState, log=True):
            '''Overload of the superclass method to add event
               emission functionality.
            '''
            if self.get_state() != newState:
                if log:
                    self.warn_stream(
                        "Change state from {0} to {1}"
                        "".format(self.get_state(), newState))
                tango.Device_4Impl.set_state(self, newState)
                self.push_change_event('State', newState)
                self.set_status("")
                # as this changes the state, clean non important
                # messages in status

        def set_status(self, newLine2status, important=False):
            '''Overload of the superclass method to add the extra feature of
               the persistent messages added to the status string.
            '''
            # self.debug_stream("In set_status()")
            newStatus = ""  # The device is in %s state.\n"%(self.get_state())
            for importantMsg in self._important_logs:
                if len(importantMsg) > 0:
                    newStatus = "%s%s\n" % (newStatus, importantMsg)
            if len(newLine2status) > 0 and \
                    newLine2status not in self._important_logs:
                newStatus = "%s%s\n" % (newStatus, newLine2status)
                if important:
                    self._important_logs.append(newLine2status)
            if len(newStatus) == 0:
                newStatus = "The device is in %s state.\n" % (self.get_state())
            oldStatus = self.get_status()
            if newStatus != oldStatus:
                tango.Device_4Impl.set_status(self, newStatus)
                self.warn_stream("New status message: {0}"
                                 "".format(repr(self.get_status())))
                self.push_change_event('Status', newStatus)

        def clean_status(self):
            '''With the extra feature of the important logs, this method allows
               to clean all those logs as a clean interlocks method does.
            '''
            self.debug_stream("In clean_status()")
            self._important_logs = []
            self.set_status("")
        # done state/status manager methods ---

        ####
        # event methods ---
        def fireEvent(self, attrEventStruct, timestamp=None):
            '''Method with the procedure to emit an event from one existing
               attribute. Minimal needs are the attribute name and the value
               to emit, but also can be specified the quality and the timestamp
            '''
            attrName = attrEventStruct[0]
            if attrName not in ['lastUpdate', 'lastUpdateStatus']:
                self.warn_stream("DEPRECATED: fireEvent({0})".format(attrName))
            attrValue = attrEventStruct[1]
            if timestamp is None:
                timestamp = time.time()
            if len(attrEventStruct) == 3:  # the quality is specified
                quality = attrEventStruct[2]
            else:
                quality = tango.AttrQuality.ATTR_VALID
            if self.__isHistoryBuffer(attrName):
                attrValue = self.__buildHistoryBufferString(attrName)
                self.push_change_event(attrName, attrValue, timestamp, quality)
            else:
                self.push_change_event(attrName, attrValue, timestamp, quality)
            attrStruct = self._getAttrStruct(attrName)
            if attrStruct is not None and \
                    LASTEVENTQUALITY in attrStruct and \
                    not quality == attrStruct[LASTEVENTQUALITY]:
                attrStruct[LASTEVENTQUALITY] = quality
            if attrStruct is not None and EVENTTIME in attrStruct:
                now = time.time()
                attrStruct[EVENTTIME] = now
                attrStruct[EVENTTIMESTR] = time.ctime(now)

        def fireEventsList(self, eventsAttrList, timestamp=None, log=False):
            '''Given a set of pair [attr,value] (with an optional third element
               with the quality) emit events for all of them with the same
               timestamp.
            '''
            if log:
                self.debug_stream(
                    "In fireEventsList(): {0:d} events:\n{1}".format(
                        len(eventsAttrList),
                        ''.join("\t{0}\n".format(line)
                                for line in eventsAttrList)))
            if timestamp is None:
                timestamp = time.time()
            attrNames = []
            for attrEvent in eventsAttrList:
                try:
                    self.fireEvent(attrEvent, timestamp)
                    attrNames.append(attrEvent[0])
                except Exception as exc:
                    self.error_stream(
                        "In fireEventsList() Exception with attribute {0}: "
                        "{1}".format(attrEvent, exc))
                    traceback.print_exc()
        # done event methods ---

        ####
        # Read Attr method for dynattrs ---
        def _getAttrStruct(self, attrName):
            '''Given an attribute name, return the internal structure that
               defines its behaviour.
            '''
            try:
                return self._plcAttrs[
                    self.__getDctCaselessKey(attrName, self._plcAttrs)]
            except ValueError as e:
                pass  # simply was not in the plcAttrs
            try:
                return self._internalAttrs[
                    self.__getDctCaselessKey(attrName, self._internalAttrs)]
            except ValueError as e:
                pass  # simply was not in the internalAttrs
            if attrName.count('_'):
                mainName, suffix = attrName.rsplit('_', 1)
                try:
                    return self._internalAttrs[
                        self.__getDctCaselessKey(mainName,
                                                 self._internalAttrs)]
                except ValueError as e:
                    pass  # simply was not in the internalAttrs
            return None

        def __getDctCaselessKey(self, key, dct):
            position = [e.lower() for e in dct].index(key.lower())
            return dct.keys()[position]

        @AttrExc
        def read_attr(self, attr):
            '''
            '''
            if self.get_state() == tango.DevState.FAULT or \
                    not self.has_data_available():
                return  # raise AttributeError("Not available in fault state!")
            name = attr.get_name()
            attrStruct = self._getAttrStruct(name)
            if any([isinstance(attrStruct, kls)
                    for kls in [
                        PLCAttr, InternalAttr, EnumerationAttr,
                        EnumerationParameter, MeaningAttr, HistoryAttr,
                        AutoStopAttr, AutoStopParameter, GroupAttr]]):
                attrStruct.read_attr(attr)
                return

        def read_lock(self):
            '''
            '''
            if self.get_state() == tango.DevState.FAULT or \
                    not self.has_data_available():
                return  # raise AttributeError("Not available in fault state!")
            rbyte = self.read_db.b(self.locking_raddr)
            locker = bool(rbyte & (1 << self.locking_rbit))
            return locker

        @AttrExc
        def read_Locking(self, attr):
            '''The read of this attribute is a boolean to represent if the
               control of the plc has been take by tango. This doesn't look
               to correspond exactly with the same meaning of the "Local Lock"
               boolean in the memory map of the plc'''
            if self.get_state() == tango.DevState.FAULT or \
                    not self.has_data_available():
                return  # raise AttributeError("Not available in fault state!")
            self._checkLocking()
            attrName = attr.get_name()
            value, timestamp, quality = self._plcAttrs[attrName].vtq
            attr.set_value_date_quality(value, timestamp, quality)

        @AttrExc
        def read_Lock_ST(self, attr):
            '''
            '''
            if self.get_state() == tango.DevState.FAULT or \
                    not self.has_data_available():
                return  # raise AttributeError("Not available in fault state!")
            attrName = attr.get_name()
            self.info_stream('DEPRECATED: reading %s' % (attrName))
            value, timestamp, quality = self._plcAttrs[attrName].vtq
            attr.set_value_date_quality(value, timestamp, quality)

        def _checkLocking(self):
            if self._isLocalLocked() or self._isRemoteLocked():
                self._lockingChange(True)
            else:
                self._lockingChange(False)

        def _isLocalLocked(self):
            return self._deviceIsInLocal and \
                self._plcAttrs['Lock_ST'].rvalue == 1

        def _isRemoteLocked(self):
            return self._deviceIsInRemote and \
                self._plcAttrs['Lock_ST'].rvalue == 2

        def _lockingChange(self, newLockValue):
            if self.is_lockedByTango != newLockValue:
                if 'Locking' in self._plcAttrs:
                    self._plcAttrs['Locking'].read_value = newLockValue
                self.is_lockedByTango = newLockValue
        # # Read Attr method for dynattrs ---

        ####
        # Write Attr method for dynattrs ---
        def prepare_write(self, attr):
            '''
            '''
            self.warn_stream(": prepare_write(%s)"
                             % (attr.get_name()))
            data = []
            self.Locking.get_write_value(data)
            val = data[0]
            if attr.get_name().lower() in ['locking']:
                self.debug_stream("Do not do the write checks, when what is "
                                  "wanted is to write the locker")
                # FIXME: perhaps check if it is already lock by another program
            elif not self.read_lock():
                try:
                    exceptionMsg = 'first required to set Locking flag on '\
                        '%s device' % self.get_name()
                except Exception as e:
                    self.error_stream("Exception in prepare_write(): %s" % (e))
                else:
                    raise LinacException(exceptionMsg)
            if self.tainted:
                raise LinacException('mismatch with '
                                     'specification:\n'+self.tainted)
            data = []
            attr.get_write_value(data)
            return data[0]

        @AttrExc
        def write_attr(self, attr):
            '''
            '''
            if self.get_state() == tango.DevState.FAULT or \
                    not self.has_data_available():
                return  # raise AttributeError("Not available in fault state!")
            name = attr.get_name()
            attrStruct = self._getAttrStruct(name)
            if any([isinstance(attrStruct, kls)
                    for kls in [PLCAttr, InternalAttr, EnumerationAttr,
                                EnumerationParameter, MeaningAttr,
                                AutoStopAttr, AutoStopParameter, GroupAttr]]):
                attrStruct.write_attr(attr)
                return

        def _getPlcUpdatePeriod(self):
            try:
                return self._plcUpdatePeriod
            except AttributeError:
                return PLC_MAX_UPDATE_PERIOD

        def _setPlcUpdatePeriod(self, value):
            self.info_stream(
                "modifying PLC Update period: was {0:.3f} and "
                "now becomes {1:.3f}.".format(self._plcUpdatePeriod, value))
            self._plcUpdatePeriod = value
        # done autostop area ---

        def __isHistoryBuffer(self, attrName):
            attrStruct = self._getAttrStruct(attrName)
            if attrStruct is not None and BASESET in attrStruct and \
                    type(attrStruct[READVALUE]) == HistoryBuffer:
                return True
            return False

        def loadAttrFile(self):
            self.attr_loaded = True
            if self.AttrFile:
                attr_fname = self.AttrFile
            else:
                attr_fname = self.get_name().split('/')[-1]+'.py'
            try:
                self.attr_list.build(attr_fname.lower())
            except Exception as exc:
                if self.get_state() != tango.DevState.FAULT:
                    self.set_state(tango.DevState.FAULT)
                    self.set_status("ReloadAttrFile() failed ({0})"
                                    "".format(exc), important=True)

        @AttrExc
        def read_lastUpdateStatus(self, attr):
            '''
            '''
            if self.get_state() == tango.DevState.FAULT or \
                    not self.has_data_available():
                return  # raise AttributeError("Not available in fault state!")
            attr.set_value(self.read_lastUpdateStatus_attr)

        @AttrExc
        def read_lastUpdate(self, attr):
            '''
            '''
            if self.get_state() == tango.DevState.FAULT or \
                    not self.has_data_available():
                return  # raise AttributeError("Not available in fault state!")
            attr.set_value(self.read_lastUpdate_attr)

        # Done Write Attr method for dynattrs ---

        # PROTECTED REGION END --- linacdata.global_variables

        def __init__(self, cl, name):
                tango.Device_4Impl.__init__(self, cl, name)
                self.log = self.get_logger()
                LinacData.init_device(self)

        def delete_device(self):
                self.info_stream('deleting device '+self.get_name())
                self._plcUpdateJoiner.set()
                self._tangoEventsJoiner.set()
                self._newDataAvailable.set()
                self.attr_list.remove_all()

        def init_device(self):
            try:
                self.debug_stream("In "+self.get_name()+"::init_device()")
                self.set_change_event('State', True, False)
                self.set_change_event('Status', True, False)
                self.attr_IsSayAgainEnable_read = False
                self.attr_IsTooFarEnable_read = True
                self.attr_forceWriteDB_read = ""
                self.attr_DynamicAttributes_errors_read = ""
                self.attr_cpu_percent_read = 0.0
                self.attr_mem_percent_read = 0.0
                self.attr_mem_rss_read = 0
                self.attr_mem_swap_read = 0
                # The attributes Locking, Lock_ST, and HeartBeat have also
                # events but this call is made in each of the AttrList method
                # who dynamically build them.
                self.set_state(tango.DevState.INIT)
                self.set_status('inititalizing...')
                self.get_device_properties(self.get_device_class())
                self.debug_stream('AttrFile='+str(self.AttrFile))
                self._locals = {'self': self}
                self._globals = globals()
                # String with human infomation about the last update
                self.read_lastUpdateStatus_attr = ""
                attr = tango.Attr('lastUpdateStatus',
                                    tango.DevString, tango.READ)
                attrProp = tango.UserDefaultAttrProp()
                attrProp.set_label('Last Update Status')
                attr.set_default_properties(attrProp)
                self.add_attribute(attr, r_meth=self.read_lastUpdateStatus)
                self.set_change_event('lastUpdateStatus', True, False)
                # numeric attr about the lapsed time of the last update
                self.read_lastUpdate_attr = None
                attr = tango.Attr('lastUpdate',
                                    tango.DevDouble, tango.READ)
                attrProp = tango.UserDefaultAttrProp()
                attrProp.set_format(latin1('%f'))
                attrProp.set_label('Last Update')
                attrProp.set_unit('s')
                attr.set_default_properties(attrProp)
                self.add_attribute(attr, r_meth=self.read_lastUpdate)
                self.set_change_event('lastUpdate', True, False)

                self._process = psutil.Process()

                self.attr_list = AttrList(self)

                ########
                # region to setup the network communication parameters

                # restrictions and rename of PLC's ip address
                if self.IpAddress == '' and self.PlcAddress == '':
                    self.error_stream("The PLC ip address must be set")
                    self.set_state(tango.DevState.FAULT)
                    self.set_status("Please set the PlcAddress property",
                                    important=True)
                    return
                elif not self.IpAddress == '' and self.PlcAddress == '':
                    self.warn_stream("Deprecated property IpAddress, "
                                     "please use PlcAddress")
                    self.PlcAddress = self.IpAddress
                elif not self.IpAddress == '' and not self.PlcAddress == '' \
                        and not self.IpAddress == self.PlcAddress:
                    self.warn_stream("Both PlcAddress and IpAddress "
                                     "properties are defined and with "
                                     "different values, prevail PlcAddress")

                # get the ip address of the host where the device is running
                # this to know if the device is running in local or remote
                try:
                    thisHostIp = get_ip(self.EthernetInterface)
                except IOError as exc:
                    self.error_stream("Cannot get the ip address of the "
                                      "interface {}".format(
                        self.EthernetInterface))
                    thisHostIp = None
                if not thisHostIp == self.BindAddress:
                    if not self.BindAddress == '':
                        self.warn_stream("BindAddress property defined but "
                                         "deprecated and it doesn't match "
                                         "with the host where device runs. "
                                         "Overwrite BindAddress with '%s'"
                                         % thisHostIp)
                    else:
                        self.debug_stream("BindAddress of this host '%s'"
                                          % (thisHostIp))
                    self.BindAddress = thisHostIp

                # check if the port corresponds to local and remote modes
                if thisHostIp == self.LocalAddress:
                    self.info_stream('Connection to the PLC will be '
                                     'local mode')
                    self.set_status('Connection in local mode', important=True)
                    self._deviceIsInLocal = True
                    self._deviceIsInRemote = False
                    try:
                        if self.LocalPort is not None:
                            self.info_stream('Using specified local port %s'
                                             % (self.LocalPort))
                            self.Port = self.LocalPort
                        else:
                            self.warn_stream('Local port not specified, '
                                             'trying to use deprecated '
                                             'definition')
                            if self.Port > 2010:
                                self.Port -= 10
                                self.warn_stream('converted the port to local'
                                                 ' %s' % self.Port)
                    except Exception as exc:
                        self.error_stream('Error in the local port setting: {0}'
                                          ''.format(exc))
                elif thisHostIp == self.RemoteAddress:
                    self.info_stream('Connection to the PLC with be '
                                     'remote mode')
                    self.set_status('Connection in remote mode',
                                    important=True)
                    self._deviceIsInLocal = False
                    self._deviceIsInRemote = True
                    try:
                        if self.RemotePort is not None:
                            self.info_stream('Using specified remote port %s'
                                             % (self.RemotePort))
                            self.Port = self.RemotePort
                        else:
                            self.warn_stream('Remote port not specified, '
                                             'trying to use deprecated '
                                             'definition')
                            if self.Port < 2010:
                                self.Port += 10
                                self.warn_stream('converted the port to '
                                                 'remote %s'
                                                 % (self.RemotePort))
                    except Exception as exc:
                        self.error_stream('Error in the remote port setting {0}'
                                          ''.format(exc))
                else:
                    self.warn_stream('Unrecognized IP for local/remote '
                                     'modes (%s)' % thisHostIp)
                    self.set_status('Unrecognized connection for local/remote'
                                    ' mode', important=True)
                    self._deviceIsInLocal = False
                    self._deviceIsInRemote = False

                # restrictions and renames of the Port's properties
                if self.Port is None:
                    self.debug_stream("The PLC ip port must be set")
                    self.set_state(tango.DevState.FAULT)
                    self.set_status("Please set the plc ip port",
                                    important=True)
                    return

                # end the region to setup the network communication parameters
                ########

                if self.ReadSize <= 0 or self.WriteSize <= 0:
                    self.set_state(tango.DevState.FAULT)
                    self.set_status("Block Read/Write sizes not well "
                                    "set (r=%d,w=%d)" % (self.ReadSize,
                                                         self.WriteSize),
                                    important=True)
                    return

                # true when reading some attribute failed....
                self.tainted = ''
                # where the readback of the set points begins
                self.offset_sp = self.ReadSize-self.WriteSize

                self.attr_loaded = False
                self.last_update_time = time.time()

                try:
                    self.connect()
                except Exception:
                    traceback.print_exc()
                    self.disconnect()
                    self.set_state(tango.DevState.UNKNOWN)
                self.info_stream('initialized')
                # self.set_state(tango.DevState.UNKNOWN)

                self._threadingBuilder()
            except Exception:
                self.error_stream('initialization failed')
                self.debug_stream("{0}".format(traceback.format_exc()))
                self.set_state(tango.DevState.FAULT)
                self.set_status("{0}".format(traceback.format_exc()))

        # --------------------------------------------------------------------
        #    linacdata read/write attribute methods
        # --------------------------------------------------------------------

        # PROTECTED REGION ID(linacdata.initialize_dynamic_attributes) ---
        def initialize_dynamic_attributes(self):
            self.loadAttrFile()
            self.attr_list._fileParsed.wait()
            self._threadingStart()            
            self.info_stream("with all the attributes build, proceed...")

        # PROTECTED REGION END --- linacdata.initialize_dynamic_attributes

        # ------------------------------------------------------------------
        #    Read EventsTime attribute
        # ------------------------------------------------------------------
        def read_EventsTime(self, attr):
            # self.debug_stream("In " + self.get_name() + ".read_EventsTime()")
            # PROTECTED REGION ID(linacdata.EventsTime_read) --
            self.attr_EventsTime_read = self._tangoEventsTime.array
            # PROTECTED REGION END --- linacdata.EventsTime_read
            attr.set_value(self.attr_EventsTime_read)

        # ------------------------------------------------------------------
        #    Read EventsTimeMix attribute
        # ------------------------------------------------------------------
        def read_EventsTimeMin(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsTimeMin()")
            # PROTECTED REGION ID(linacdata.EventsTimeMin_read) --
            self.attr_EventsTimeMin_read = self._tangoEventsTime.array.min()
            if self._tangoEventsTime.array.size < HISTORY_EVENT_BUFFER:
                attr.set_value_date_quality(self.attr_EventsTimeMin_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_CHANGING)
                return
            # PROTECTED REGION END --- linacdata.EventsTimeMin_read
            attr.set_value(self.attr_EventsTimeMin_read)

        # ------------------------------------------------------------------
        #    Read EventsTimeMax attribute
        # ------------------------------------------------------------------
        def read_EventsTimeMax(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsTimeMax()")
            # PROTECTED REGION ID(linacdata.EventsTimeMax_read) --
            self.attr_EventsTimeMax_read = self._tangoEventsTime.array.max()
            if self._tangoEventsTime.array.size < HISTORY_EVENT_BUFFER:
                attr.set_value_date_quality(self.attr_EventsTimeMax_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_CHANGING)
                return
            elif self.attr_EventsTimeMax_read >= self._getPlcUpdatePeriod()*3:
                attr.set_value_date_quality(self.attr_EventsTimeMax_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_WARNING)
                return
            # PROTECTED REGION END --- linacdata.EventsTimeMax_read
            attr.set_value(self.attr_EventsTimeMax_read)

        # ------------------------------------------------------------------
        #    Read EventsTimeMean attribute
        # ------------------------------------------------------------------
        def read_EventsTimeMean(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsTimeMean()")
            # PROTECTED REGION ID(linacdata.EventsTimeMean_read) --
            self.attr_EventsTimeMean_read = self._tangoEventsTime.array.mean()
            if self._tangoEventsTime.array.size < HISTORY_EVENT_BUFFER:
                attr.set_value_date_quality(self.attr_EventsTimeMean_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_CHANGING)
                return
            elif self.attr_EventsTimeMean_read >= self._getPlcUpdatePeriod():
                attr.set_value_date_quality(self.attr_EventsTimeMean_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_WARNING)
                return
            # PROTECTED REGION END --- linacdata.EventsTimeMean_read
            attr.set_value(self.attr_EventsTimeMean_read)

        # ------------------------------------------------------------------
        #    Read EventsTimeStd attribute
        # ------------------------------------------------------------------
        def read_EventsTimeStd(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsTimeStd()")
            # PROTECTED REGION ID(linacdata.EventsTimeStd_read) --
            self.attr_EventsTimeStd_read = self._tangoEventsTime.array.std()
            if self._tangoEventsTime.array.size < HISTORY_EVENT_BUFFER:
                attr.set_value_date_quality(self.attr_EventsTimeStd_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_CHANGING)
                return
            # PROTECTED REGION END --- linacdata.EventsTimeStd_read
            attr.set_value(self.attr_EventsTimeStd_read)

        # ------------------------------------------------------------------
        #    Read EventsNumber attribute
        # ------------------------------------------------------------------
        def read_EventsNumber(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsNumber()")
            # PROTECTED REGION ID(linacdata.EventsNumber_read) ---
            self.attr_EventsNumber_read = self._tangoEventsNumber.array
            # PROTECTED REGION END --- linacdata.EventsNumber_read
            attr.set_value(self.attr_EventsNumber_read)

        # ------------------------------------------------------------------
        #    Read EventsNumberMin attribute
        # ------------------------------------------------------------------
        def read_EventsNumberMin(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsNumberMin()")
            # PROTECTED REGION ID(linacdata.EventsNumberMin_read) ---
            self.attr_EventsNumberMin_read = \
                int(self._tangoEventsNumber.array.min())
            if self._tangoEventsNumber.array.size < HISTORY_EVENT_BUFFER:
                attr.set_value_date_quality(self.attr_EventsNumberMin_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_CHANGING)
                return
            # PROTECTED REGION END --- linacdata.EventsNumberMin_read
            attr.set_value(self.attr_EventsNumberMin_read)

        # ------------------------------------------------------------------
        #    Read EventsNumberMax attribute
        # ------------------------------------------------------------------
        def read_EventsNumberMax(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsNumberMax()")
            # PROTECTED REGION ID(linacdata.EventsNumberMax_read) ---
            self.attr_EventsNumberMax_read = \
                int(self._tangoEventsNumber.array.max())
            if self._tangoEventsNumber.array.size < HISTORY_EVENT_BUFFER:
                attr.set_value_date_quality(self.attr_EventsNumberMax_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_CHANGING)
                return
            # PROTECTED REGION END --- linacdata.EventsNumberMax_read
            attr.set_value(self.attr_EventsNumberMax_read)

        # ------------------------------------------------------------------
        #    Read EventsNumberMean attribute
        # ------------------------------------------------------------------
        def read_EventsNumberMean(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsNumberMean()")
            # PROTECTED REGION ID(linacdata.EventsNumberMean_read) ---
            self.attr_EventsNumberMean_read = \
                self._tangoEventsNumber.array.mean()
            if self._tangoEventsNumber.array.size < HISTORY_EVENT_BUFFER:
                attr.set_value_date_quality(self.attr_EventsNumberMean_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_CHANGING)
                return
            # PROTECTED REGION END --- linacdata.EventsNumberMean_read
            attr.set_value(self.attr_EventsNumberMean_read)

        # ------------------------------------------------------------------
        #    Read EventsNumberStd attribute
        # ------------------------------------------------------------------
        def read_EventsNumberStd(self, attr):
            # self.debug_stream("In " + self.get_name() +
            #                   ".read_EventsNumberStd()")
            # PROTECTED REGION ID(linacdata.EventsNumberStd_read) ---
            self.attr_EventsNumberStd_read = \
                self._tangoEventsNumber.array.std()
            if self._tangoEventsNumber.array.size < HISTORY_EVENT_BUFFER:
                attr.set_value_date_quality(self.attr_EventsNumberStd_read,
                                            time.time(),
                                            tango.AttrQuality.ATTR_CHANGING)
                return
            # PROTECTED REGION END --- linacdata.EventsNumberStd_read
            attr.set_value(self.attr_EventsNumberStd_read)

        # ------------------------------------------------------------------
        #    Read IsTooFarEnable attribute
        # ------------------------------------------------------------------
        def read_IsTooFarEnable(self, attr):
            self.debug_stream("In " + self.get_name() +
                              ".read_IsTooFarEnable()")
            # PROTECTED REGION ID(linacdata.IsTooFarEnable_read) ---

            # PROTECTED REGION END --- linacdata.IsTooFarEnable_read
            attr.set_value(self.attr_IsTooFarEnable_read)

        # ------------------------------------------------------------------
        #    Write IsTooFarEnable attribute
        # ------------------------------------------------------------------
        def write_IsTooFarEnable(self, attr):
            self.debug_stream("In " + self.get_name() +
                              ".write_IsTooFarEnable()")
            data = attr.get_write_value()
            # PROTECTED REGION ID(linacdata.IsTooFarEnable_write) ---
            self.attr_IsTooFarEnable_read = bool(data)
            # PROTECTED REGION END -- linacdata.IsTooFarEnable_write

        # ------------------------------------------------------------------
        #    Read forceWriteDB attribute
        # ------------------------------------------------------------------
        def read_forceWriteDB(self, attr):
            self.debug_stream("In " + self.get_name() +
                              ".read_forceWriteDB()")
            # PROTECTED REGION ID(linacdata.forceWriteDB_read) ---

            # PROTECTED REGION END --- linacdata.forceWriteDB_read
            attr.set_value(self.attr_forceWriteDB_read)

        # ------------------------------------------------------------------
        #    Read cpu_percent attribute
        # ------------------------------------------------------------------
        def read_cpu_percent(self, attr):
            self.debug_stream("In " + self.get_name() +
                              ".read_cpu_percent()")
            # PROTECTED REGION ID(linacdata.cpu_percent_read) ---
            self.attr_cpu_percent_read = self._process.cpu_percent()
            # PROTECTED REGION END --- linacdata.cpu_percent_read
            attr.set_value(self.attr_cpu_percent_read)

        # ------------------------------------------------------------------
        #    Read mem_percent attribute
        # ------------------------------------------------------------------
        def read_mem_percent(self, attr):
            self.debug_stream("In " + self.get_name() +
                              ".read_mem_percent()")
            # PROTECTED REGION ID(linacdata.mem_percent_read) ---
            self.attr_mem_percent_read = self._process.memory_percent()
            # PROTECTED REGION END --- linacdata.mem_percent_read
            attr.set_value(self.attr_mem_percent_read)

        # ------------------------------------------------------------------
        #    Read mem_rss attribute
        # ------------------------------------------------------------------
        def read_mem_rss(self, attr):
            self.debug_stream("In " + self.get_name() +
                              ".read_mem_rss()")
            # PROTECTED REGION ID(linacdata.mem_rss_read) ---
            self.attr_mem_rss_read = self._process.memory_info().rss
            # PROTECTED REGION END --- linacdata.mem_rss_read
            attr.set_value(self.attr_mem_rss_read)

        # ------------------------------------------------------------------
        #    Read mem_swap attribute
        # ------------------------------------------------------------------
        def read_mem_swap(self, attr):
            self.debug_stream("In " + self.get_name() +
                              ".read_mem_swap()")
            # PROTECTED REGION ID(linacdata.mem_swap_read) ---
            self.attr_mem_swap_read = self._process.memory_full_info().swap
            # PROTECTED REGION END --- linacdata.mem_swap_read
            attr.set_value(self.attr_mem_swap_read)

        # ------------------------------------------------------------------
        #    Read DynamicAttributes_errors attribute
        # ------------------------------------------------------------------
        def read_DynamicAttributes_errors(self, attr):
            self.debug_stream("In " + self.get_name() +
                              ".read_DynamicAttributes_errors()")
            # PROTECTED REGION ID(linacdata.DynamicAttributes_errors_read) ---

            # PROTECTED REGION END --- linacdata.DynamicAttributes_errors_read
            attr.set_value(self.attr_DynamicAttributes_errors_read)

        # ---------------------------------------------------------------------
        #    linacdata command methods
        # ---------------------------------------------------------------------
        @CommandExc
        def ReloadAttrFile(self):
            """Reload the file containing the attr description for a
               particular plc

            :param argin:
            :type: tango.DevVoid
            :return:
            :rtype: tango.DevVoid """
            self.debug_stream('In ReloadAttrFile()')
            # PROTECTED REGION ID(linacdata.ReloadAttrFile) ---
            self.loadAttrFile()
            # PROTECTED REGION END --- linacdata.ReloadAttrFile

        @CommandExc
        def Exec(self, cmd):
            """ Direct command to execute python with in the device, use it
                very carefully it's good for debuging but it's a security
                thread.

            :param argin:
            :type: tango.DevString
            :return:
            :rtype: tango.DevString """
            self.debug_stream('In Exec()')
            # PROTECTED REGION ID(linacdata.Exec) ---
            L = self._locals
            G = self._globals
            try:
                if cmd.count("=") == 1:
                    # assignment, interpretation as statement
                    try:
                        exec (cmd, G, L)
                        result = L.get("y")
                    except Exception as error:
                        self.error_stream("Exception executing {!r} -> {}: {}"
                                          "".format(cmd, type(error), error))
                        raise error
                else:  # interpretation as expression
                    try:
                        result = eval(cmd, G, L)
                    except Exception as error:
                        self.warn_stream("Exception evaluating {!r} -> {}: {}"
                                         "".format(cmd, type(error), error))
                        raise error
            except Exception as error:
                # handles errors on both eval and exec level
                result = error

            if type(result) == StringType:
                return result
            elif isinstance(result, BaseException):
                return "%s!\n%s" % (result.__class__.__name__, str(result))
            else:
                return pprint.pformat(result)
            # PROTECTED REGION END --- linacdata.Exec

        @CommandExc
        def GetBit(self, args):
            """ Command to direct Read a bit position from the PLC memory

            :param argin:
            :type: tango.DevVarShortArray
            :return:
            :rtype: tango.DevBoolean """
            self.debug_stream('In GetBit()')
            # PROTECTED REGION ID(linacdata.GetBit) ---
            idx, bitno = args
            if self.read_db is not None and hasattr(self.read_db, 'bit'):
                return self.read_db.bit(idx, bitno)
            raise IOError("No access to the hardware")
            # PROTECTED REGION END --- linacdata.GetBit

        @CommandExc
        def GetByte(self, idx):
            """Command to direct Read a byte position from the PLC memory

            :param argin:
            :type: tango.DevShort
            :return:
            :rtype: tango.DevShort """
            self.debug_stream('In GetByte()')
            # PROTECTED REGION ID(linacdata.GetByte) ---
            if self.read_db is not None and hasattr(self.read_db, 'b'):
                return self.read_db.b(idx)
            raise IOError("No access to the hardware")
            # PROTECTED REGION END --- linacdata.GetByte

        @CommandExc
        def GetShort(self, idx):
            """Command to direct Read two consecutive byte positions from the
               PLC memory and understand it as an integer

            :param argin:
            :type: tango.DevShort
            :return:
            :rtype: tango.DevShort """
            self.debug_stream('In GetShort()')
            # PROTECTED REGION ID(linacdata.GetShort)  ---
            if self.read_db is not None and hasattr(self.read_db, 'i16'):
                return self.read_db.i16(idx)
            raise IOError("No access to the hardware")
            # PROTECTED REGION END --- LinacBData.GetShort

        @CommandExc
        def GetFloat(self, idx):
            """ Command to direct Read four consecutive byte positions from the
                PLC memory and understand it as an float

            :param argin:
            :type: tango.DevShort
            :return:
            :rtype: tango.DevFloat """
            self.debug_stream('In GetFloat()')
            # PROTECTED REGION ID(linacdata.GetFloat) ---
            if self.read_db is not None and hasattr(self.read_db, 'f'):
                return self.read_db.f(idx)
            raise IOError("No access to the hardware")
            # PROTECTED REGION END --- linacdata.GetFloat

        @CommandExc
        def HexDump(self):
            """ Hexadecimal dump of all the registers in the plc

            :param argin:
            :type: tango.DevVoid
            :return:
            :rtype: tango.DevString """
            self.debug_stream('In HexDump()')
            # PROTECTED REGION ID(linacdata.HexDump) ---
            rblock = self.read_db.buf[:]
            wblock = self.write_db.buf[self.write_db.write_start:]
            return hex_dump([rblock, wblock])
            # PROTECTED REGION END --- linacdata.HexDump

        @CommandExc
        def Hex(self, idx):
            """ Hexadecimal dump the given register of the plc

            :param argin:
            :type: tango.DevShort
            :return:
            :rtype: tango.DevString """
            self.debug_stream('In Hex()')
            # PROTECTED REGION ID(linacdata.Hex) ---
            return hex(self.read_db.b(idx))
            # PROTECTED REGION END --- linacdata.Hex

        @CommandExc
        def DumpTo(self, arg):
            """ Hexadecimal dump of all the registers in the plc to a file

            :param argin:
            :type: tango.DevString
            :return:
            :rtype: tango.DevVoid """
            self.debug_stream('In DumpTo()')
            # PROTECTED REGION ID(linacdata.DumpTo) ---
            fout = open(arg, 'w')
            fout.write(self.read_db.buf.tostring())
            # PROTECTED REGION END --- linacdata.DumpTo

        @CommandExc
        def WriteBit(self, args):
            """ Write a single bit in the memory of the plc [reg,bit,value]

            :param argin:
            :type: tango.DevVarShortArray
            :return:
            :rtype: tango.DevVoid """
            self.debug_stream('In WriteBit()')
            # PROTECTED REGION ID(linacdata.WriteBit) ---
            idx, bitno, v = args
            idx += bitno / 8
            bitno %= 8
            v = bool(v)
            b = self.write_db.b(idx)  # Get the byte where the bit is
            b = b & ~(1 << bitno) | (v << bitno)
            # change only the expected bit
            # The write operation of a bit, writes the Byte where it is
            self.write_db.write(idx, b, TYPE_MAP[tango.DevUChar])
            # PROTECTED REGION END --- linacdata.WriteBit

        @CommandExc
        def WriteByte(self, args):
            """ Write a byte in the memory of the plc [reg,value]

            :param argin:
            :type: tango.DevVarShortArray
            :return:
            :rtype: tango.DevVoid """
            self.debug_stream('In WriteByte()')
            # PROTECTED REGION ID(linacdata.WriteByte) ---
            # args[1] = c_uint8(args[1])
            register = args[0]
            value = uint8(args[1])
            # self.write_db.write( *args )
            self.write_db.write(register, value, TYPE_MAP[tango.DevUChar])
            # PROTECTED REGION END --- linacdata.WriteByte

        @CommandExc
        def WriteShort(self, args):
            """ Write two consecutive bytes in the memory of the plc
               [reg,value]

            :param argin:
            :type: tango.DevVarShortArray
            :return:
            :rtype: tango.DevVoid """
            self.debug_stream('In WriteShort()')
            # PROTECTED REGION ID(linacdata.WriteShort) ---
            # args[1] = c_int16(args[1])
            register = args[0]
            value = int16(args[1])
            # self.write_db.write( *args )
            self.write_db.write(register, value, TYPE_MAP[tango.DevShort])
            # PROTECTED REGION END --- linacdata.WriteShort

        @CommandExc
        def WriteFloat(self, args):
            """ Write the representation of a float in four consecutive bytes
                in the memory of the plc [reg,value]

            :param argin:
            :type: tango.DevVarShortArray
            :return:
            :rtype: tango.DevVoid """
            self.debug_stream('In WriteFloat()')
            # PROTECTED REGION ID(linacdata.WriteFloat) ---
            idx = int(args[0])
            f = float32(args[1])
            self.write_db.write(idx, f, TYPE_MAP[tango.DevFloat])
            # PROTECTED REGION END --- linacdata.WriteFloat

        @CommandExc
        def ResetState(self):
            """ Clean the information set in the Status message and restore
                the state

            :param argin:
            :type: tango.DevVoid
            :return:
            :rtype: tango.DevVoid """
            self.debug_stream('In ResetState()')
            # PROTECTED REGION ID(linacdata.ResetState) ---
            self.info_stream('resetting state %s...' % str(self.get_state()))
            if self.get_state() == tango.DevState.FAULT:
                if self.disconnect():
                    self.set_state(tango.DevState.OFF)  # self.connect()
            elif self.is_connected():
                self.set_state(tango.DevState.ON)
                self.clean_status()
            else:
                self.set_state(tango.DevState.UNKNOWN)
                self.set_status("")
            # PROTECTED REGION END --- linacdata.ResetState

        @CommandExc
        def RestoreReadDB(self):
            self.forceWriteAttrs()

        @CommandExc
        def updateDynamicAttributes(self):
            """
            Passes the content in the DynamicAttributes property to the
            internal skippy object to inject some extra attributes defined in
            the property.

            :param :
            :type: tango.DevVoid
            :return:
            :rtype: tango.DevBoolean
            """
            self.debug_stream("In updateDynamicAttributes()")
            try:
                # reload properties from db
                self.info_stream("(re)Load the properties from the data base")
                self.get_device_properties(self.get_device_class())
                property_str = ""
                with_line_number = ""
                for i, element in enumerate(self.DynamicAttributes):
                    property_str += "{0}\n".format(element)
                    with_line_number += "{0}: {1}\n".format(i, element)
                self.info_stream("Preparing to parse:\n{0}"
                                 "".format(with_line_number))
                self.attr_list.parse(property_str)
                self.info_stream("parse complete without exceptions")
                self.attr_DynamicAttributes_errors_read = ""
                argout = True
            except Exception as exc:
                self.error_stream("updateDynamicAttributes() didn't end well:\n"
                                  "{0}".format(exc))
                self.error_stream("{0}".format(traceback.format_exc()))
                self.attr_DynamicAttributes_errors_read = str(exc)
                argout = False
            return argout

        # To be moved ---
        def _threadingBuilder(self):
            self.info_stream("Building Threads and Events")
            # Threading joiners ---
            self._plcUpdateJoiner = threading.Event()
            self._plcUpdateJoiner.clear()
            self._tangoEventsJoiner = threading.Event()
            self._tangoEventsJoiner.clear()
            # Threads declaration ---
            self._plcUpdateThread = \
                threading.Thread(name="PlcUpdater",
                                 target=self.plcUpdaterThread)
            self._tangoEventsThread = \
                threading.Thread(name="EventManager",
                                 target=self.newValuesThread)
            self._tangoEventsTime = \
                CircularBuffer([], maxlen=HISTORY_EVENT_BUFFER, owner=None)
            self._tangoEventsNumber = \
                CircularBuffer([], maxlen=HISTORY_EVENT_BUFFER, owner=None)
            # Threads configuration ---
            self._plcUpdateThread.setDaemon(True)
            self._tangoEventsThread.setDaemon(True)
            self._plcUpdatePeriod = PLC_MAX_UPDATE_PERIOD
            self._newDataAvailable = threading.Event()
            self._newDataAvailable.clear()

        def _threadingStart(self):
            if not hasattr(self, '_plcUpdateThread') or \
                    not hasattr(self, '_tangoEventsThread'):
                self.error_stream("Devices not correctly initialized to start "
                                  "threads. Check for previous errors")
            else:
                # Launch those threads ---
                self._plcUpdateThread.start()
                self._tangoEventsThread.start()

        def plcUpdaterThread(self):
            '''
            '''
            while not self._plcUpdateJoiner.isSet():
                try:
                    start_t = time.time()
                    if self.is_connected():
                        self._readPlcRegisters()
                        self._addaptPeriod(time.time()-start_t)
                    else:
                        if self._plcUpdateJoiner.isSet():
                            return
                        self.info_stream('plc not connected')
                        self.reconnect()
                        time.sleep(self.ReconnectWait)
                except Exception as e:
                    self.error_stream("In plcUpdaterThread() "
                                      "exception: %s" % (e))
                    traceback.print_exc()

        def _readPlcRegisters(self):
            """ Do a read of all the registers in the plc and update the
                mirrored memory

            :param argin:
            :type: tango.DevVoid
            :return:
            :rtype: tango.DevVoid """

            # faults are critical and can not be recovered by restarting things
            # INIT states mean something is going is on that interferes with
            #      updating, such as connecting
            start_update_time = time.time()
            if (self.get_state() == tango.DevState.FAULT) or \
                    not self.is_connected():
                if start_update_time - self.last_update_time \
                        < self.ReconnectWait:
                    return
                else:
                    if self.connect():
                        self.set_state(tango.DevState.UNKNOWN)
                    return
            # relock if auto-recover from fault ---
            try:
                self.auto_local_lock()
                self.dataBlockSemaphore.acquire()
                try:
                    e = None
                    # The real reading to the hardware:
                    up = self.read_db.readall()
                except Exception as e:
                    self.error_stream(
                        "Could not complete the readall()\n%s" % (e))
                finally:
                    self.dataBlockSemaphore.release()
                    if e is not None:
                        raise e
                if up:
                    self.last_update_time = time.time()
                    if self.get_state() == tango.DevState.ALARM:
                        # This warning would be because attributes with
                        # this quality, don't log because it happens too often.
                        self.set_state(tango.DevState.ON, log=False)
                    if not self.get_state() in [tango.DevState.ON]:
                        # Recover a ON state when it is responding and the
                        # state was showing something different.
                        self.set_state(tango.DevState.ON)
                else:
                    self.set_state(tango.DevState.FAULT)
                    self.set_status("No data received from the PLC")
                    self.disconnect()
                end_update_t = time.time()
                diff_t = (end_update_t - start_update_time)
                if end_update_t-self.last_update_time > self.TimeoutAlarm:
                    self.set_state(tango.DevState.ALARM)
                    self.set_status("Timeout alarm!")
                    return
                # disconnect if no new information is send after long time
                if end_update_t-self.last_update_time > self.TimeoutConnection:
                    self.disconnect()
                    self.set_state(tango.DevState.FAULT)
                    self.set_status("Timeout connection!")
                    return
                self.read_lastUpdate_attr = diff_t
                timeFormated = time.strftime('%F %T')
                self.read_lastUpdateStatus_attr = \
                    "last updated at {0} in {1:f} s".format(timeFormated,
                                                            diff_t)
                attr2Event = [['lastUpdate', self.read_lastUpdate_attr],
                              ['lastUpdateStatus',
                               self.read_lastUpdateStatus_attr]]
                self.fireEventsList(attr2Event,
                                    timestamp=self.last_update_time)
                self._newDataAvailable.set()
                # when an update goes fine, the period is reduced one step
                # until the minumum
                if self._getPlcUpdatePeriod() > PLC_MIN_UPDATE_PERIOD:
                    self._setPlcUpdatePeriod(self._plcUpdatePeriod -
                                             PLC_STEP_UPDATE_PERIOD)
            except tcpblock.Shutdown as exc:
                self.set_state(tango.DevState.FAULT)
                msg = 'communication shutdown requested '\
                      'at '+time.strftime('%F %T')
                self.set_status(msg)
                self.error_stream(msg)
                self.disconnect()
            except socket.error as exc:
                self.set_state(tango.DevState.FAULT)
                msg = 'broken socket at {0}\n{1}'.format(time.strftime('%F %T'),
                                                         str(exc))
                self.set_status(msg)
                self.error_stream(msg)
                self.disconnect()
            except Exception as exc:
                self.set_state(tango.DevState.FAULT)
                msg = 'update failed at {0}\n{1}: {2}' \
                      ''.format(time.strftime('%F %T'), str(type(exc)),
                                str(exc))
                self.set_status(msg)
                self.error_stream(msg)
                self.disconnect()
                self.last_update_time = time.time()
                traceback.print_exc()

        def _addaptPeriod(self, diff_t):
            current_p = self._getPlcUpdatePeriod()
            max_t = PLC_MAX_UPDATE_PERIOD
            step_t = PLC_STEP_UPDATE_PERIOD
            if diff_t > max_t:
                if current_p < max_t:
                    self.warn_stream(
                        "plcUpdaterThread() has take too much time "
                        "({0:3.3f} seconds)".format(diff_t))
                    self._setPlcUpdatePeriod(current_p+step_t)
                else:
                    self.error_stream(
                        "plcUpdaterThread() has take too much time "
                        "({0:3.3f} seconds), but period cannot be increased "
                        "more than {1:3.3f} seconds".format(diff_t, current_p))
            elif diff_t > current_p:
                exceed_t = diff_t-current_p
                factor = int(exceed_t/step_t)
                increment_t = step_t+(step_t*factor)
                if current_p+increment_t >= max_t:
                    self.error_stream(
                        "plcUpdaterThread() it has take {0:3.6f} seconds "
                        "({1:3.6f} more than expected) and period will be "
                        "increased to the maximum ({2:3.6f})"
                        "".format(diff_t, exceed_t, max_t))
                    self._setPlcUpdatePeriod(max_t)
                else:
                    self.warn_stream(
                        "plcUpdaterThread() it has take {0:3.6f} seconds, "
                        "{1:f} over the expected, increase period "
                        "({2:3.3f} + {3:3.3f} seconds)"
                        "".format(diff_t, exceed_t, current_p, increment_t))
                    self._setPlcUpdatePeriod(current_p+increment_t)
            else:
                # self.debug_stream(
                #     "plcUpdaterThread() it has take {0:3.6f} seconds, going to "
                #     "sleep {1:3.3f} seconds (update period {2:3.3f} seconds)"
                #     "".format(diff_t, current_p-diff_t, current_p))
                time.sleep(current_p-diff_t)

        def newValuesThread(self):
            '''
            '''
            if not self.attr_list._fileParsed.isSet():
                self.info_stream("Event generator thread will wait until "
                                 "file is parsed")
            self.attr_list._fileParsed.wait()
            while not self.has_data_available():
                time.sleep(self._getPlcUpdatePeriod()*2)
                self.debug_stream("Event generator thread wait for connection")
            event_ctr = EventCtr()
            while not self._tangoEventsJoiner.isSet():
                try:
                    if self._newDataAvailable.isSet():
                        start_t = time.time()
                        self.propagateNewValues()
                        diff_t = time.time() - start_t
                        n_events = event_ctr.ctr
                        event_ctr.clear()
                        self._tangoEventsTime.append(diff_t)
                        self._tangoEventsNumber.append(n_events)
                        if n_events > 0:
                            self.debug_stream(
                                "newValuesThread() it has take {0:3.6f} "
                                "seconds for {1:d} events".format(diff_t,
                                                                  n_events))
                        self._newDataAvailable.clear()
                    else:
                        self._newDataAvailable.wait()
                except Exception as exc:
                    self.error_stream(
                        "In newValuesThread() exception: {0}".format(exc))
                    traceback.print_exc()

        def propagateNewValues(self):
            """
Check the attributes that comes directly from the PLC registers, to check if
the information stored in the device needs to be refresh, events emitted, as
well as for each of them, inter-attribute dependencies are required to be
triggered.
            """
            attrs = self._plcAttrs.keys()[:]
            for attrName in attrs:
                attrStruct = self._plcAttrs[attrName]
                if hasattr(attrStruct, 'hardwareRead'):
                    attrStruct.hardwareRead(self.read_db)

        def auto_local_lock(self):
            if self._deviceIsInLocal:
                if 'Locking' in self._plcAttrs:
                    if not self._plcAttrs['Locking'].rvalue:
                        self.info_stream("Device is in Local mode and "
                                         "not locked. Proceed to lock it")
                        with self.dataBlockSemaphore:
                            self.relock()
                            time.sleep(self._getPlcUpdatePeriod())
                    # else:
                    #     self.info_stream("Device is in Local mode and locked")
                else:
                    self.warn_stream("Device in Local mode but 'Locking' "
                                     "attribute not yet present")
            # else:
            #     self.info_stream("Device is not in Local mode")

        def relock(self):
            '''
            '''
            if 'Locking' in self._plcAttrs and \
                    not self._plcAttrs['Locking'].wvalue:
                self.write_lock(True)
        # end "To be moved" section

        def write_lock(self, value):
            '''
            '''

            if self.get_state() == tango.DevState.FAULT or \
                    not self.has_data_available():
                return  # raise AttributeError("Not available in fault state!")
            if not isinstance(value, bool):
                raise ValueError("write_lock argument must be a boolean")
            if 'Locking' in self._plcAttrs:
                raddr = self._plcAttrs['Locking'].read_addr
                rbit = self._plcAttrs['Locking'].read_bit
                rbyte = self.read_db.b(raddr)
                waddr = self._plcAttrs['Locking'].write_addr
                if value:
                    # sets bit 'bitno' of b
                    toWrite = rbyte | (int(value) << rbit)
                    # a byte of 0s with a unique 1 in the place to set this 1
                else:
                    # clears bit 'bitno' of b
                    toWrite = rbyte & (0xFF) ^ (1 << rbit)
                    # a byte of 1s with a unique 0 in the place to set this 0
                self.write_db.write(waddr, toWrite, TYPE_MAP[tango.DevUChar])
                time.sleep(self._getPlcUpdatePeriod())
                reRead = self.read_db.b(raddr)
                self.info_stream(
                    "Writing Locking boolean to {0} ({1:d}.{2:d}) byte "
                    "was {3}; write {4}; now {5}".format(
                        "  lock" if value else "unlock", raddr, rbit,
                        bin(rbyte), bin(toWrite), bin(reRead)))
                self._plcAttrs['Locking'].write_value = value

        @CommandExc
        def Update(self):
            '''Deprecated
            '''
            pass
            # PROTECTED REGION END --- linacdata.Update


# ==================================================================
#
#       LinacDataClass class definition
#
# ==================================================================
class LinacDataClass(tango.DeviceClass):
        # -------- Add you global class variables here ------------------------
        # PROTECTED REGION ID(linacdata.global_class_variables) ---

        # PROTECTED REGION END --- linacdata.global_class_variables

        def dyn_attr(self, dev_list):
            """Invoked to create dynamic attributes for the given devices.
            Default implementation calls
            :meth:`linacdata.initialize_dynamic_attributes` for each device

            :param dev_list: list of devices
            :type dev_list: :class:`tango.DeviceImpl`"""

            for dev in dev_list:
                try:
                    dev.initialize_dynamic_attributes()
                except Exception as exc:
                    dev.warn_stream("Failed to initialize dynamic attributes:"
                                    " {}".format(exc))
                    dev.debug_stream("Details: {0}".format(
                        traceback.format_exc()))
            # PROTECTED REGION ID(linacdata.dyn_attr) ENABLED START ---

            # PROTECTED REGION END --- linacdata.dyn_attr

        # Class Properties ---
        class_property_list = {}

        # Device Properties ---
        device_property_list = {'ReadSize': [tango.DevShort,
                                             "how many bytes to read (should "
                                             "be a multiple of 2)", 0
                                             ],
                                'WriteSize': [tango.DevShort,
                                              "size of write data block", 0],
                                'IpAddress': [tango.DevString,
                                              "ipaddress of linac PLC "
                                              "(deprecated)", ''],
                                'PlcAddress': [tango.DevString,
                                               "ipaddress of linac PLC", ''],
                                'EthernetInterface': [tango.DevString,
                                                      "interface of the "
                                                      "machine to be used to "
                                                      "connect to the plcs",
                                                      'eth0'],
                                'Port': [tango.DevShort,
                                         "port of linac PLC (deprecated)",
                                         None],
                                'LocalPort': [tango.DevShort,
                                              "port of linac PLC (deprecated)",
                                              None],
                                'RemotePort': [tango.DevShort,
                                               "port of linac PLC "
                                               "(deprecated)", None],
                                'AttrFile': [tango.DevString,
                                             "file that contains description "
                                             "of attributes of this "
                                             "Linac data block", ''],
                                'BindAddress': [tango.DevString,
                                                'ip of the interface used to '
                                                'communicate with plc '
                                                '(deprecated)', ''],
                                'LocalAddress': [tango.DevString,
                                                 'ip of the interface used '
                                                 'to communicate with plc as '
                                                 'the local', '10.0.7.100'],
                                'RemoteAddress': [tango.DevString,
                                                  'ip of the interface used '
                                                  'to communicate with plc as '
                                                  'the remote', '10.0.7.1'],
                                'TimeoutAlarm': [tango.DevDouble,
                                                 "after how many seconds of "
                                                 "silence the state is set "
                                                 "to alarm, this should be "
                                                 "less than TimeoutConnection",
                                                 1.0],
                                'TimeoutConnection': [tango.DevDouble,
                                                      "after how many seconds "
                                                      "of silence the "
                                                      "connection is assumed "
                                                      "to be interrupted",
                                                      1.5],
                                'ReconnectWait': [tango.DevDouble,
                                                  "after how many seconds "
                                                  "since the last update the "
                                                  "next connection attempt is "
                                                  "made", 6.0],
                                'DynamicAttributes':
                                    [tango.DevVarStringArray,
                                     "Way to define specific attributes on a "
                                     "single instrument. Like the ones in the "
                                     "definition file.",
                                     []],
                                }
        class_property_list['TimeoutAlarm'] = \
            device_property_list['TimeoutAlarm']
        class_property_list['TimeoutConnection'] = \
            device_property_list['TimeoutConnection']
        class_property_list['ReconnectWait'] = \
            device_property_list['ReconnectWait']

        # Command definitions ---
        cmd_list = {'ReloadAttrFile': [[tango.DevVoid, ""],
                                       [tango.DevVoid, ""]],
                    'Exec': [[tango.DevString, "statement to executed"],
                             [tango.DevString, "result"],
                             {'Display level': tango.DispLevel.EXPERT, }],
                    'GetBit': [[tango.DevVarShortArray, "idx"],
                               [tango.DevBoolean, ""],
                               {'Display level': tango.DispLevel.EXPERT, }],
                    'GetByte': [[tango.DevShort, "idx"],
                                [tango.DevShort, ""],
                                {'Display level': tango.DispLevel.EXPERT, }],
                    'GetShort': [[tango.DevShort, "idx"],
                                 [tango.DevShort, ""],
                                 {'Display level':
                                  tango.DispLevel.EXPERT, }],
                    'GetFloat': [[tango.DevShort, "idx"],
                                 [tango.DevFloat, ""],
                                 {'Display level':
                                  tango.DispLevel.EXPERT, }],
                    'HexDump': [[tango.DevVoid, "idx"],
                                [tango.DevString, "hexdump of all data"]],
                    'Hex': [[tango.DevShort, "idx"],
                            [tango.DevString, ""]],
                    'DumpTo': [[tango.DevString, "target file"],
                               [tango.DevVoid, ""], {}],
                    'WriteBit': [[tango.DevVarShortArray,
                                  "idx, bitno, value"],
                                 [tango.DevVoid, ""],
                                 {'Display level':
                                  tango.DispLevel.EXPERT, }],
                    'WriteByte': [[tango.DevVarShortArray, "idx, value"],
                                  [tango.DevVoid, ""],
                                  {'Display level':
                                   tango.DispLevel.EXPERT, }],
                    'WriteShort': [[tango.DevVarShortArray, "idx, value"],
                                   [tango.DevVoid, ""],
                                   {'Display level':
                                    tango.DispLevel.EXPERT, }],
                    'WriteFloat': [[tango.DevVarFloatArray, "idx, value"],
                                   [tango.DevVoid, ""],
                                   {'Display level':
                                    tango.DispLevel.EXPERT}],
                    'ResetState': [[tango.DevVoid, ""],
                                   [tango.DevVoid, ""]],
                    'Update': [[tango.DevVoid, ""],
                               [tango.DevVoid, ""],
                               # { 'polling period' : 50 }
                               ],
                    'RestoreReadDB': [[tango.DevVoid, ""],
                                      [tango.DevVoid, ""],
                                      {'Display level':
                                           tango.DispLevel.EXPERT}],
                    'updateDynamicAttributes':
                        [[tango.DevVoid, "none"],
                        [tango.DevBoolean, "none"]],
                    }

        # Attribute definitions ---
        attr_list = {'EventsTime': [[tango.DevDouble,
                                     tango.SPECTRUM,
                                     tango.READ, 1800],
                                    {'Display level':
                                     tango.DispLevel.EXPERT}
                                    ],
                     'EventsTimeMin': [[tango.DevDouble,
                                        tango.SCALAR,
                                        tango.READ],
                                       {'Display level':
                                        tango.DispLevel.EXPERT}
                                       ],
                     'EventsTimeMax': [[tango.DevDouble,
                                        tango.SCALAR,
                                        tango.READ],
                                       {'Display level':
                                        tango.DispLevel.EXPERT}
                                       ],
                     'EventsTimeMean': [[tango.DevDouble,
                                        tango.SCALAR,
                                        tango.READ],
                                       {'Display level':
                                        tango.DispLevel.EXPERT}
                                       ],
                     'EventsTimeStd': [[tango.DevDouble,
                                        tango.SCALAR,
                                        tango.READ],
                                       {'Display level':
                                        tango.DispLevel.EXPERT}
                                       ],
                     'EventsNumber': [[tango.DevShort,
                                       tango.SPECTRUM,
                                       tango.READ, 1800],
                                      {'Display level':
                                       tango.DispLevel.EXPERT}
                                      ],
                     'EventsNumberMin': [[tango.DevUShort,
                                          tango.SCALAR,
                                          tango.READ],
                                         {'Display level':
                                          tango.DispLevel.EXPERT}
                                         ],
                     'EventsNumberMax': [[tango.DevUShort,
                                          tango.SCALAR,
                                          tango.READ],
                                         {'Display level':
                                          tango.DispLevel.EXPERT}
                                         ],
                     'EventsNumberMean': [[tango.DevDouble,
                                          tango.SCALAR,
                                          tango.READ],
                                         {'Display level':
                                          tango.DispLevel.EXPERT}
                                         ],
                     'EventsNumberStd': [[tango.DevDouble,
                                          tango.SCALAR,
                                          tango.READ],
                                         {'Display level':
                                          tango.DispLevel.EXPERT}
                                         ],
                     'IsTooFarEnable': [[tango.DevBoolean,
                                         tango.SCALAR,
                                         tango.READ_WRITE],
                                        {'label':
                                         "Is Too Far readback Feature "
                                         "Enabled?",
                                         'Display level':
                                         tango.DispLevel.EXPERT,
                                         'description':
                                         "This boolean is to enable or "
                                         "disable the feature to use the "
                                         "quality warning for readback "
                                         "attributes with setpoint too far",
                                         'Memorized': "true"
                                         }
                                        ],
                     'forceWriteDB': [[tango.DevString,
                                       tango.SCALAR,
                                       tango.READ],
                                      {'Display level':
                                           tango.DispLevel.EXPERT}
                                      ],
                     'cpu_percent': [[tango.DevDouble,
                                      tango.SCALAR,
                                      tango.READ],
                                     {'Display level':
                                          tango.DispLevel.EXPERT}
                                     ],
                     'mem_percent': [[tango.DevDouble,
                                      tango.SCALAR,
                                      tango.READ],
                                     {'Display level':
                                          tango.DispLevel.EXPERT}
                                     ],
                     'mem_rss': [[tango.DevULong,
                                  tango.SCALAR,
                                  tango.READ],
                                 {'Display level':
                                      tango.DispLevel.EXPERT,
                                  'unit': 'Bytes'}
                                 ],
                     'mem_swap': [[tango.DevULong,
                                   tango.SCALAR,
                                   tango.READ],
                                  {'Display level':
                                       tango.DispLevel.EXPERT,
                                  'unit': 'Bytes'}
                                  ],
                     'DynamicAttributes_errors': [[tango.DevString,
                                       tango.SCALAR,
                                       tango.READ],
                                      {'Display level':
                                           tango.DispLevel.EXPERT}
                                      ],
                     }

def main():
    try:
        py = tango.Util(sys.argv)
        py.add_TgClass(LinacDataClass, LinacData, 'linacdata')
        U = tango.Util.instance()
        U.server_init()
        U.server_run()
    except tango.DevFailed as e:
        tango.Except.print_exception(e)
    except Exception as e:
        traceback.print_exc()

if __name__ == '__main__':
    main()
