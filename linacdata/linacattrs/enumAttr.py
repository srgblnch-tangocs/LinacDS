# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

from ast import literal_eval
try:
    from .abstract import AttrExc
    from .linacAttr import LinacAttr
    from .linacfeatures import Memorised
    from .linacFeatureAttr import _LinacFeatureAttr
# suse11.1 backwards compatibility
except ValueError:  # Attempted relative import in non-package
    from abstract import AttrExc
    from linacAttr import LinacAttr
    from linacfeatures import Memorised
    from linacFeatureAttr import _LinacFeatureAttr

try:
    from tango import AttrQuality, DevString, DevShort
# suse11.1 backwards compatibility
except ImportError:  # No module named tango
    from PyTango import AttrQuality, DevString, DevShort

from time import time

__author__ = "Lothar Krause and Sergi Blanch-Torne and Emilio Morales"
__maintainer__ = "Emilio Morales"
__copyright__ = "Copyright 2015, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


class EnumerationAttr(LinacAttr):
    def __init__(self, *args, **kwargs):
        """Precursor for the future CalibrationAttr class. Based on a list,
           provided by the user, that can be modified (and that's why it is
           memorised) provides a numeric access as well as a string meaning
           that combines both: the number and the string associated.
        """
        super(EnumerationAttr, self).__init__(*args, **kwargs)

        self._options = EnumerationParameterOptions(tag="Options",
                                                    dataType=DevString,
                                                    owner=self,
                                                    device=self.device,
                                                    hook=self.options_changed)
        self._active = EnumerationParameter(tag="Active", dataType=DevString,
                                            owner=self,
                                            device=self.device,
                                            hook=self.active_changed)
        self._numeric = EnumerationParameter(tag="Numeric", dataType=DevShort,
                                             owner=self,
                                             device=self.device)
        self._meaning = EnumerationParameter(tag="Meaning", dataType=DevString,
                                             owner=self,
                                             device=self.device)

        self.debug("Build the EnumerationAttr %s" % self.name)

    @property
    def options(self):
        """List of the options in the enumeration
        """
        return self._options.value

    @property
    def active(self):
        """Indicates which of the options is activated by the user selection.
        """
        if self._active is None:
            return 'None'
        return self._active.value

    @property
    def numeric(self):
        """Machine-friendly output with the element active.
        """
        if self._active.value is None:
            return 0
        return self._numeric.value

    @property
    def meaning(self):
        """Humane-friendly output with the element active.
        """
        if self._active.value is None:
            return 'None'
        return self._meaning.value

    def _build_numeric_and_meaning(self):
        if self._options.value and self._active.value:
            if self._active.value in self._options.as_list:
                self._numeric.rvalue = self._options.as_list.index(
                    self._active.value)+1
                self._meaning.rvalue = "%d:%s" % (self.numeric,
                                                  self._active.value)
            else:
                self._numeric.resetValue()
                self._meaning.resetValue()
            if self._eventsObj:
                self._eventsObj.fireEvent()

    def options_changed(self):
        self.info("Options changed: %s" % self._options.value)
        self._build_numeric_and_meaning()

    def active_changed(self):
        self.info("Active changed: %s" % self._active.value)
        self._build_numeric_and_meaning()


class EnumerationParameter(_LinacFeatureAttr):
    _value = None
    _tag = None
    _type = None
    _hook = None
    _write_t = None

    def __init__(self, owner, tag, dataType, hook=None, *args, **kwargs):
        super(EnumerationParameter, self).__init__(owner=owner,
                                                   name="%s:%s"
                                                        % (owner.name, tag),
                                                   valueType=dataType,
                                                   *args, **kwargs)
        self._tag = tag
        self._name = "%s(%s)" % (self._name, self._tag)
        if dataType == DevShort:
            self._type = int
        elif dataType == DevString:
            self._type = str
        else:
            self._type = dataType
        self.hook = hook

    def __str__(self):
        return "%s (%s)" % (self.alias, self.__class__.__name__)

    @property
    def value(self):
        return self.rvalue

    @property
    def rvalue(self):
        return self._value

    @rvalue.setter
    def rvalue(self, value):
        if isinstance(value, self._type):
            if self._value != value:
                self._value = value
                self._timestamp = time()
                self._event(self._tag, self.rvalue, self._timestamp)
                if self.hook:
                    self._hook()
        else:
            try:
                self.rvalue = eval("%s(%s)" % (self._type.__name__, value))
            except Exception:
                self.warning("rvalue assignment failed in the eval() section")

    def resetValue(self):
        self._value = None
        self._timestamp = time()
        self._event(self._tag, self.rvalue, self._timestamp)
        if self.hook:
            self._hook()

    def doWriteValue(self, value):
        super(EnumerationParameter, self).doWriteValue(value)
        self._write_t = time()
        self.rvalue = value  # propagate (including the hook)
        if self._memorised is not None:
            self._memorised.store("%s" % self.rvalue)

    @property
    def hook(self):
        return self._hook is not None

    @hook.setter
    def hook(self, func):
        if callable(func):
            self._hook = func

    def _event(self, suffix, value, timestamp):
        if self._owner and self._owner._eventsObj:
            attrName = "%s_%s" % (self._owner.name, suffix)
            eventsObj = self._owner._eventsObj
            if value is None:
                value = self.noneValue
            if value == self.noneValue:
                quality = AttrQuality.ATTR_INVALID
            else:
                quality = AttrQuality.ATTR_VALID
            eventsObj.fireEvent(name=attrName, value=value,
                                timestamp=timestamp, quality=quality)


class EnumerationParameterOptions(EnumerationParameter):
    def __init__(self, *args, **kwargs):
        super(EnumerationParameterOptions, self).__init__(*args, **kwargs)

    @property
    def rvalue(self):
        if self._value:
            return ', '.join(self._value)
        else:
            return str(self._value)

    @rvalue.setter
    def rvalue(self, value):
        if isinstance(value, self._type):
            try:
                value = [v.strip() for v in value.split(',')]
            except SyntaxError:
                raise SyntaxError("Options format is not valid.")
            if self._value != value:
                self._value = value
                self._timestamp = time()
                self._event(self._tag, self.rvalue, self._timestamp)
                if self.hook:
                    self._hook()
        else:
            try:
                self.rvalue = eval("%s(%s)" % (self._type.__name__, value))
            except Exception:
                self.warning("rvalue assignment failed in the eval() section")

    @property
    def noneValue(self):
        return "None"

    @property
    def as_list(self):
        return self._value
