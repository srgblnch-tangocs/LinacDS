# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

try:
    from .abstract import (LinacException, CommandExc, AttrExc, binaryByte,
                           hex_dump)
    from .autostopAttr import AutoStopAttr, AutoStopParameter
    from .enumAttr import EnumerationAttr, EnumerationParameter
    from .groupAttr import GroupAttr
    from .historyAttr import HistoryAttr
    from .internalAttr import InternalAttr
    from .linacAttr import LinacAttr
    from .linacAttrBase import LinacAttrBase
    from .logicAttr import LogicAttr
    from .meaningAttr import MeaningAttr
    from .plcAttr import PLCAttr
# suse11.1 backwards compatibility
except ValueError:  # Attempted relative import in non-package
    from abstract import (LinacException, CommandExc, AttrExc, binaryByte,
                          hex_dump)
    from autostopAttr import AutoStopAttr, AutoStopParameter
    from enumAttr import EnumerationAttr, EnumerationParameter
    from groupAttr import GroupAttr
    from historyAttr import HistoryAttr
    from internalAttr import InternalAttr
    from linacAttr import LinacAttr
    from linacAttrBase import LinacAttrBase
    from logicAttr import LogicAttr
    from meaningAttr import MeaningAttr
    from plcAttr import PLCAttr

__author__ = "Lothar Krause and Sergi Blanch-Torne and Emilio Morales"
__maintainer__ = "Emilio Morales"
__copyright__ = "Copyright 2017, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"
